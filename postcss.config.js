const autoprefixerPlugin = require('autoprefixer');
const cssnanoPlugin = require('cssnano');
const { config: dotenvConfig } = require('dotenv');

dotenvConfig();

const isProdEnv = process.env.NODE_ENV === 'production';

const postcssConfig = {
  plugins: [],
};

if (isProdEnv) {
  postcssConfig.plugins.push(
    autoprefixerPlugin(),
    cssnanoPlugin(),
  );
}

module.exports = postcssConfig;
