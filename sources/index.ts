import { AxiosBoundary } from 'modules/components';
import { PortalProvider } from 'modules/portal';
import { StoreProvider } from 'crunch-store';
import { createElement } from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import { App } from 'main/app';
import { rootStore } from 'main/store';

import './styles/index.scss';
import './vendors';

document.addEventListener('DOMContentLoaded', () => {
  const rootApp = createElement(App);
  const browserRouter = createElement(BrowserRouter, {}, rootApp);
  const storeProvider = createElement(StoreProvider, { store: rootStore }, browserRouter);
  const axiosBoundary = createElement(AxiosBoundary, {}, storeProvider);
  const dialogProvider = createElement(PortalProvider, {}, axiosBoundary);

  render(dialogProvider, document.getElementById('root-app'));
});

// window.addEventListener('error', (event: ErrorEvent) => {
//   event.preventDefault();
//   console.error(event);
// });
