import React, { createContext, Fragment, ReactNode, useContext, useReducer } from 'react';

type PortalAction =
  { type: 'show', payload: { component: ReactNode, id: string } } |
  { type: 'hide', payload: { id: string } };

const PortalContext = createContext(undefined);
const portalState = {};

const portalReducer = (state = portalState, action: PortalAction) => {
  switch (action.type) {
    case 'show': return { ...state, [action.payload.id]: action.payload.component };
    case 'hide': return { ...state, [action.payload.id]: undefined };
    default: return state;
  }
};

export function PortalProvider(props) {
  const [state, dispatch] = useReducer(portalReducer, portalState);

  const portalChildren = Object
    .values(state)
    .map((node: ReactNode, index) => (<Fragment key={index}>{node}</Fragment>));

  return (
    <PortalContext.Provider value={[state, dispatch]}>
      {props.children}
      {portalChildren}
    </PortalContext.Provider>
  );
}

export function usePortal(component: ReactNode, id: string = Math.random().toString()) {
  // TODO неочень хорошо, но что поделать
  const context = PortalContext.Provider['_context']['_currentValue']/*useContext(PortalContext)*/;

  const hide = (): void => context[1]({ type: 'hide', payload: { id } });
  const show = (): void => context[1]({ type: 'show', payload: { component, id } });
  const toggle = (value: boolean = !context[0][id]): void => value ? show() : hide();

  return { hide, show, toggle, visible: '' };
}
