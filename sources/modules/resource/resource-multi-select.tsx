import { ListItemProps, MultiSelect } from '@progress/kendo-react-dropdowns';
import { TagData } from '@progress/kendo-react-dropdowns/dist/es/MultiSelect/TagList';
import { get, debounce } from 'lodash';
import { ResourceItemProps } from 'modules/resource/resource-item';
import React, { cloneElement, Fragment, ReactElement, useEffect, useState } from 'react';

import { ResourceControlsProps } from 'modules/resource/resource-controls';
import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceMultiSelectProps<T> {
  controls?: boolean;
  controlsRender?: (props: ResourceControlsProps<T>) => ReactElement;
  disabled?: boolean;
  itemRender?: (props: ResourceItemProps<T>) => ReactElement;
  required?: boolean;
  service: ResourceService<T>;
  value?: ResourceMultiSelectProps<T>['valuePrimitive'] extends true ? number[] : T[];
  valuePrimitive?: boolean;
  onChange?: (event: { value: ResourceMultiSelectProps<T>['value'] }) => void;
}

export function ResourceMultiSelect<T>(props: ResourceMultiSelectProps<T>): ReactElement {
  const [data, setData] = useState<T[]>();
  const [filterValue, setFilterValue] = useState<string>();
  const [loading, setLoading] = useState<boolean>();
  const [value, setValue] = useState<T[]>();
  const { dataItemKey, scope, textField } = props.service.options;
  const getUpsertValue = () => props.valuePrimitive ? value.map((v) => v[dataItemKey]) : value;

  const fetchData = () => {
    setLoading(true);

    props
      .service
      .query({ scope, ['where.like-' + textField]: filterValue })
      .finally(() => setLoading(false))
      .then((response) => setData(response.data));
  };

  const fetchValue = () => {
    if (props.valuePrimitive && props.value.length > 0) {
      setLoading(true);

      props
        .service
        .query({ scope, ['where.or-' + dataItemKey]: props.value.join(',') })
        .finally(() => setLoading(false))
        .then((response) => setValue(response.data));
    } else {
      setValue(props.value as T[]);
    }
  };

  const itemRender = (li: ReactElement<HTMLLIElement>, itemProps: ListItemProps): ReactElement => {
    return cloneElement(li, li.props, (
      <>
        {props.itemRender == null && get(itemProps.dataItem, textField)}
        {props.itemRender?.({ service: props.service, value: itemProps.dataItem })}
      </>
    ));
  };

  const tagRender = (tagData: TagData, li: ReactElement<HTMLLIElement>): ReactElement => {
    const closeProps = (li.props.children[1] as object as ReactElement).props;

    return cloneElement(li, { ...li.props, className: 'k-button p-0' }, (
      <div className="btn-group btn-group-sm">
        <button className="btn btn-secondary" disabled type="button">
          {tagData.text}
        </button>

        {(props.controls ?? true) && props.controlsRender?.({
          extract: true,
          onUpsert: (e) => handleControlsUpsert(e),
          service: props.service,
          value: tagData.data[0],
        })}

        <button className="btn btn-secondary" type="button" onClick={closeProps.onClick}>
          <i className="fas fa-times"/>
        </button>
      </div>
    ));
  };

  const handleControlsUpsert = (event: { value: T }) => {
    const index = value?.findIndex((v) => v[dataItemKey] === event.value[dataItemKey]) ?? -1;

    if (index !== -1) {
      Object.assign(value[index], event.value);
      setValue([...value]);
    } else if (value != null) {
      value.push(event.value);
    } else {
      setValue([ event.value ]);
    }

    setValue([...value]);
    fetchData();
  };

  useEffect(() => fetchData(), [filterValue]);
  useEffect(() => fetchValue(), [props.value, props.valuePrimitive]);
  useEffect(() => value != null ? props.onChange?.({ value: getUpsertValue() }) : undefined, [value]);

  return (
    <div className="input-group">
      <MultiSelect
        data={data}
        dataItemKey={dataItemKey}
        disabled={props.disabled}
        filterable
        itemRender={itemRender}
        loading={loading}
        required={props.required}
        tagRender={tagRender}
        textField={textField}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onFilterChange={debounce((e) => setFilterValue(e.filter.value), 500)}
      />

      {(props.controls ?? true) && props.controlsRender != null && (
        <div className="input-group-append">
          {props.controlsRender({
            extract: true,
            onUpsert: (e) => handleControlsUpsert(e),
            service: props.service,
            value: undefined,
          })}
        </div>
      )}
    </div>
  );
}
