import React, { Dispatch, ReactElement, SetStateAction, SyntheticEvent, useState } from 'react';

import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceFormChildrenProps<T> {
  submitted: boolean;
  value: T;
  setSubmitted: Dispatch<SetStateAction<boolean>>;
  setValue: Dispatch<SetStateAction<T>>;
}

export interface ResourceFormProps<T> {
  children?: (props: ResourceFormChildrenProps<T>) => ReactElement;
  service: ResourceService<T>;
  value?: T;
  onUpsert?: (event: { value: T }) => void;
}

export function ResourceForm<T>(props: ResourceFormProps<T>): ReactElement {
  const [value, setValue] = useState<T>({ ...props.value });
  const [submitted, setSubmitted] = useState<boolean>();

  const handleFormSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    setSubmitted(true);

    props
      .service
      .upsert(value)
      .finally(() => setSubmitted(false))
      .then((response) => props.onUpsert?.({ value: response }));
  };

  const handleRemove = (event: SyntheticEvent) => {
    event.preventDefault();
    setSubmitted(true);

    props.service
      .remove(value)
      .finally(() => setSubmitted(false))
      .then((response) => props.onUpsert?.({ value: response }));
  };

  return (
    <form className={submitted ? 'loading' : ''} onSubmit={handleFormSubmit}>
      {props.children?.({ submitted, value, setSubmitted, setValue })}

      <div className="btn-group w-100">
        <button className="btn btn-primary w-100" type="submit">
          <i className="fas fa-save"/> Сохранить
        </button>

        <button className="btn btn-danger w-100" type="button" onClick={handleRemove}>
          <i className="fas fa-trash"/> Удалить
        </button>
      </div>
    </form>
  );
}
