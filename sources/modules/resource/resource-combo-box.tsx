import { ComboBox, ListItemProps } from '@progress/kendo-react-dropdowns';
import { debounce, get } from 'lodash';
import React, { cloneElement, Fragment, ReactElement, useEffect, useState } from 'react';

import { ResourceControlsProps } from 'modules/resource/resource-controls';
import { ResourceItemProps } from 'modules/resource/resource-item';
import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceComboBoxProps<T> {
  controls?: boolean;
  controlsRender?: (props: ResourceControlsProps<T>) => ReactElement;
  disabled?: boolean;
  itemRender?: (props: ResourceItemProps<T>) => ReactElement;
  params?: object;
  required?: boolean;
  service: ResourceService<T>;
  value?: ResourceComboBoxProps<T>['valuePrimitive'] extends true ? number : T;
  valuePrimitive?: boolean;
  onChange?: (event: { value: ResourceComboBoxProps<T>['value'] }) => void;
}

export function ResourceComboBox<T>(props: ResourceComboBoxProps<T>): ReactElement {
  const [data, setData] = useState<T[]>();
  const [filterValue, setFilterValue] = useState<string>();
  const [loading, setLoading] = useState<boolean>();
  const [value, setValue] = useState<T>();
  const { dataItemKey, scope, textField } = props.service.options;
  const getUpsertValue = () => props.valuePrimitive ? value[dataItemKey] : value;

  const fetchData = () => {
    setLoading(true);

    props
      .service
      .query({ scope, ['where.like-' + textField]: filterValue, ...props.params })
      .finally(() => setLoading(false))
      .then((response) => setData(response.data));
  };

  const fetchValue = () => {
    if (props.valuePrimitive && typeof props.value === 'number') {
      setLoading(true);

      props
        .service
        .get(props.value as number, { scope })
        .finally(() => setLoading(false))
        .then((response) => setValue(response));
    } else {
      setValue(props.value as T);
    }
  };

  const itemRender = (li: ReactElement<HTMLLIElement>, itemProps: ListItemProps): ReactElement => {
    return cloneElement(li, li.props, (
      <Fragment>
        {props.itemRender == null && get(itemProps.dataItem, textField)}
        {props.itemRender?.({ service: props.service, value: itemProps.dataItem })}
      </Fragment>
    ));
  };

  const handleControlsUpsert = (event: { value: T }) => {
    setValue({ ...value, ...event.value });
    fetchData();
  };

  useEffect(() => fetchData(), [filterValue, props.params]);
  useEffect(() => fetchValue(), [props.params, props.value, props.valuePrimitive]);
  useEffect(() => value != null ? props.onChange?.({ value: getUpsertValue() }) : undefined, [value]);

  return (
    <div className="input-group">
      <ComboBox
        data={data}
        dataItemKey={dataItemKey}
        disabled={props.disabled}
        filterable
        itemRender={itemRender}
        loading={loading}
        required={props.required}
        textField={textField}
        value={value}
        onChange={(e) => setValue(e.target.value)}
        onFilterChange={debounce((e) => setFilterValue(e.filter.value), 500)}
      />

      {(props.controls ?? true) && props.controlsRender != null && (
        <div className="input-group-append">
          {props.controlsRender({
            extract: true,
            onUpsert: (e) => handleControlsUpsert(e),
            service: props.service,
            value,
          })}
        </div>
      )}
    </div>
  );
}
