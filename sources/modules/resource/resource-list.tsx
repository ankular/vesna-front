import { PageChangeEvent, Pager } from '@progress/kendo-react-data-tools';
import { DropDownList } from '@progress/kendo-react-dropdowns';
import { Input } from '@progress/kendo-react-inputs';
import React, { ReactElement, SyntheticEvent, useEffect, useState } from 'react';

import { ResourceServiceList } from 'modules/resource/resource-service';
import { ResourceControlsProps } from 'modules/resource/resource-controls';
import { ResourceItemProps } from 'modules/resource/resource-item';
import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceListProps<T> {
  controlsRender?: (props: ResourceControlsProps<T>) => ReactElement;
  itemRender?: (props: ResourceItemProps<T>) => ReactElement;
  searchParams?: URLSearchParams;
  service: ResourceService<T>;
  onItemUpsert?: (event: { value: T }) => void;
  onSearchParamsChange?: (event: { searchParams: URLSearchParams }) => void;
}

export function ResourceList<T>(props: ResourceListProps<T>): ReactElement {
  // TODO подумать над удалением filterValue из searchParams если undefined
  const [filterValue, setFilterValue] = useState<string>();
  const [loading, setLoading] = useState<boolean>();
  const [response, setResponse] = useState<ResourceServiceList<T>>();
  const { scope, textField } = props.service.options;

  const handlePagerPageChange = (event: PageChangeEvent) => {
    const searchParams = { ...props.searchParams };

    searchParams['pager.skip'] = event.skip;
    searchParams['pager.take'] = event.take;
    props.onSearchParamsChange?.({ searchParams });
  };

  const handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault();

    const searchParams = { ...props.searchParams };

    searchParams['where.like-' + textField] = filterValue;
    props.onSearchParamsChange?.({ searchParams });
  };

  useEffect(() => {
    setLoading(true);

    props
      .service
      .query({ scope, ...props.searchParams })
      .finally(() => setLoading(false))
      .then(setResponse);
  }, [props.searchParams]);

  useEffect(() => {
    setFilterValue(props.searchParams['where.like-' + textField]);
  }, [props.searchParams['where.like-' + textField]]);

  return (
    <div className={loading ? 'loading' : ''}>
      <form className="mb-3" onSubmit={handleSubmit}>
        <div className="input-group">
          <Input
            placeholder="Поиск..."
            value={filterValue}
            onChange={(e) => setFilterValue(e.target.value)}
            onKeyPress={(e) => e.key === 'Enter' && handleSubmit(e)}
          />

          <div className="input-group-append">
            <button className="btn btn-secondary" type="submit" onClick={() => setFilterValue(undefined)}>
              <i className="fas fa-times-circle"/>
            </button>

            {/*<DropDownList*/}
            {/*  defaultItem={{ dataItemKey: '1', textField: 'Все' }}*/}
            {/*  data={[{ dataItemKey: '2', textField: 'Только удаленные' }]}*/}
            {/*  dataItemKey="dataItemKey"*/}
            {/*  textField="textField"*/}
            {/*/>*/}

            <button className="btn btn-secondary" type="submit">
              <i className="fas fa-search"/>
            </button>
          </div>
        </div>
      </form>

      <ul className="list-group">
        <li className="d-flex justify-content-between list-group-item">
          {props.itemRender?.({ service: props.service })}
          {props.controlsRender?.({ service: props.service, onUpsert: props.onItemUpsert })}
        </li>

        {response?.data.map((dataItem, index) => (
          <li className="d-flex justify-content-between list-group-item" key={index}>
            {props.itemRender?.({ service: props.service, value: dataItem })}
            {props.controlsRender?.({ service: props.service, value: dataItem, onUpsert: props.onItemUpsert })}
          </li>
        ))}
      </ul>

      <hr/>

      {response != null && (
        <Pager
          previousNext
          skip={response.skip}
          take={response.take}
          total={response.total}
          onPageChange={handlePagerPageChange}
        />
      )}
    </div>
  );
}
