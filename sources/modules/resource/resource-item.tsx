import { get } from 'lodash';
import React, { createElement, Fragment, ReactElement } from 'react';

import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceItemProps<T> {
  extract?: boolean;
  children?: (event: { value: T }) => ReactElement;
  service: ResourceService<T>;
  value?: T;
}

export function ResourceItem<T>(props: ResourceItemProps<T>): ReactElement {
  const { textField } = props.service.options;

  const wrapper = (children: ReactElement) => props.extract
    ? createElement(Fragment, {}, children)
    : createElement('div', { className: 'align-items-center d-flex justify-content-between' }, children);

  return wrapper((
    <Fragment>
      {props.children == null && (<span>{get(props.value, textField) ?? 'Без имени'}</span>)}
      {props.children?.({ value: props.value })}
    </Fragment>
  ));
}
