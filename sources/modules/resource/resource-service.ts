import axios, { AxiosRequestConfig } from 'axios';

export interface ResourceServiceList<T> {
  data: T[];
  skip: number;
  take: number;
  total: number;
}

export interface ResourceServiceOptions<T> {
  baseURL: string;
  dataItemKey: string;
  scope?: string,
  textField: string;
}

export interface ResourceService<T> {
  options: ResourceServiceOptions<T>;
  create(body: T): Promise<T>;
  get(primaryKey: number | string, params?: URLSearchParams | Record<string, string> | string ): Promise<T>;
  query(params?: URLSearchParams | Record<string, string> | string): Promise<ResourceServiceList<T>>;
  remove(body: T): Promise<T>;
  update(body: T): Promise<T>;
  upsert(body: T): Promise<T>;
}

export function createResourceService<T>(options: ResourceServiceOptions<T>): ResourceService<T> {
  return {
    options,
    create(body: T): Promise<T> {
      return axios
        .post<T>(options.baseURL, body)
        .then(({ data }) => data);
    },
    get(primaryKey: number | string, params?: URLSearchParams | string): Promise<T> {
      const config: AxiosRequestConfig = {
        params: typeof params === 'string' ? new URLSearchParams(params) : params,
      };

      return axios
        .get<T>(`${options.baseURL}/${primaryKey}`, config)
        .then(({ data }) => data);
    },
    query(params?: URLSearchParams | string): Promise<ResourceServiceList<T>> {
      const config: AxiosRequestConfig = {
        params: typeof params === 'string' ? new URLSearchParams(params) : params,
      };

      return axios
        .get<ResourceServiceList<T>>(options.baseURL, config)
        .then(({ data }) => data);
    },
    remove(body: T): Promise<T> {
      return axios
        .delete<T>(`${options.baseURL}/${body[options.dataItemKey]}`)
        .then(({ data }) => data);
    },
    update(body: T): Promise<T> {
      return axios
        .put<T>(`${options.baseURL}/${body[options.dataItemKey]}`, body)
        .then(({ data }) => data);
    },
    upsert(body: T): Promise<T> {
      return body[options.dataItemKey] ? this.update(body) : this.create(body);
    },
  };
}
