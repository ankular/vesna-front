import { Dialog } from '@progress/kendo-react-dialogs';
import { usePortal } from 'modules/portal';
import React, { createElement, Fragment, ReactElement } from 'react';

import { ResourceFormProps } from 'modules/resource/resource-form';
import { ResourceService } from 'modules/resource/resource-service';

export interface ResourceControlsProps<T> {
  children?: (props: { value: T }) => ReactElement;
  disabled?: boolean;
  extract?: boolean;
  formRender?: (props: ResourceFormProps<T>) => ReactElement;
  service: ResourceService<T>;
  value?: T;
  onUpsert?: (event: { value: T }) => void;
}

export function ResourceControls<T>(props: ResourceControlsProps<T>): ReactElement {
  const { baseURL, dataItemKey } = props.service.options;

  const formDialog = usePortal((
    <Dialog title="Редактор" onClose={() => formDialog.hide()}>
      {props.formRender?.({
        onUpsert: (e) => handleFormUpsert(e),
        service: props.service,
        value: props.value,
      })}
    </Dialog>
  ), `FormDialogUrl=${baseURL}`); // TODO подумать над name

  const handleFormUpsert = (event: { value: T }) => {
    formDialog.hide();
    props.onUpsert?.(event);
  };

  const wrapper = (children: ReactElement) => props.extract
    ? createElement(Fragment, {}, children)
    : createElement('div', { className: 'btn-group btn-group-sm' }, children);

  return wrapper((
    <Fragment>
      {props.children?.({ value: props.value })}

      <button
        className="btn btn-secondary"
        disabled={props.disabled}
        type="button"
        onClick={() => formDialog.show()}
      >
        <i className={`fas fa-${props.value?.[dataItemKey] ? 'edit' : 'plus'}`}/>
      </button>
    </Fragment>
  ));
}
