import { DependencyList, EffectCallback, useEffect, useRef } from 'react';
import { isEqual } from 'lodash';

const useDeepMemoize = (deps?: DependencyList) => {
  const ref = useRef<DependencyList>();

  if (!isEqual(deps, ref.current)) {
    ref.current = deps;
  }

  return ref.current;
};

export function useDeepEffect(effect: EffectCallback, deps?: DependencyList) {
  useEffect(effect, useDeepMemoize(deps));
}
