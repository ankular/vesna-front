import { MutableRefObject, useEffect, useState } from 'react';
import ResizeObserver from 'resize-observer-polyfill';

export function useResizeObserver<T extends HTMLElement>(target: MutableRefObject<T>) {
  const [state, setState] = useState<ResizeObserverEntry>();
  const observer = new ResizeObserver((entries) => entries.forEach((entry) => setState(entry)));

  useEffect(() => {
    observer.observe(target.current);
    return () => observer.unobserve(target.current);
  }, []);

  return state;
}
