import { matchPath, RouteProps, useLocation } from 'react-router';

export function useBreadcrumbs(routes: Array<RouteProps & { title?: string }>) {
  const { pathname } = useLocation();

  return routes
    .flatMap((route) => Array.isArray(route.path) ? route.path.map((path) => ({ ...route, path })) : route )
    .map(({ path, title }) => ({ title, url: matchPath(pathname, { path })?.url }))
    .filter(({ title, url }) => title != null && url != null)
    .sort((a, b) => a.url.split('/').length - b.url.split('/').length);
}
