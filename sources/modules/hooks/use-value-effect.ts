import { DependencyList, EffectCallback, useEffect } from 'react';

export function useValueEffect(effect: EffectCallback, deps?: DependencyList) {
  useEffect(() => {
    if (deps?.every(Boolean)) {
      return effect();
    }
  }, [deps]);
}
