export type QueryStringRules = Record<string, { isArray?: boolean, isBoolean?: boolean, isNumber?: boolean }>;

export function useQueryString() {
  return {
    objectify(init: string, rules: QueryStringRules = {}): any {
      return init
        .split(/[?&]/)
        .filter(Boolean)
        .map((part) => {
          const [key, value] = decodeURIComponent(part).split('=');

          if (key.endsWith('[]')) rules[key] = { isArray: true, ...rules[key] };

          return [key.replace(/\[]$/, ''), value.split(',').filter(Boolean)];
        })
        // группировка по ключу в массивы
        .reduce((acc, [key, value]: [string, string[]]) => {
          const index = acc.findIndex((entry) => entry[0] === key);

          if (index === -1) acc.push([key, value]);
          else acc[index][1] = [acc[index][1], value].flat();

          return acc;
        }, [] as Array<[string, string[]]>)
        // применение правил
        .reduce((acc, [key, value]: [string, string[]]) => {
          acc[key] = value;

          if (rules[key]?.isBoolean) acc[key] = acc[key].map(Boolean);
          if (rules[key]?.isNumber) acc[key] = acc[key].map(Number);

          if (rules[key]?.isArray === false || (acc[key].length <= 1 && !rules[key]?.isArray)) {
            acc[key] = acc[key].length === 1 ? acc[key][0] : acc[key].join(',');
          }

          return acc;
        }, {});
    },
    stringify(init: object, rules: QueryStringRules = {}) {
      return Object
        .entries(init)
        .map(([key, value]) => [key, [value].flat()])
        .filter(([, value]) => value.length > 0)
        // применение правил
        .flatMap(([key, value]: [string, Array<boolean | number | string>]) => {
          if (rules[key]?.isBoolean) value = value.map(Boolean);
          if (rules[key]?.isNumber) value = value.map(Number);
          if (rules[key]?.isArray === false || (value.length === 1 && !rules[key]?.isArray)) return `${key}=${value}`;
          return value.map((v) => `${key}[]=${v}`);
        })
        .join('&');
    },
  };
}
