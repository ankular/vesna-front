import { RouteProps } from 'react-router';

export function useMainRoutes(routes: Array<RouteProps & { title?: string }>) {
  return routes
    .flatMap((route) => Array.isArray(route.path) ? route.path.map((path) => ({ ...route, path })) : route )
    .map(({ path, title }: { path: string, title: string }) => ({ path, title }))
    .filter(({ path, title }) => !path?.replace(/\/admin/, '')?.replace(/^\//, '').includes('/') && title != null);
}
