/**
 * Возвращает число и слово из массива endings в зависимости от значения value
 *
 * @param endings массив слов с разными окончаниями, например ['участник', 'участника', 'участников']
 */
export function useDeclination(endings: [string, string, string]) {
  /**
   * @param value число, к которому нужно подобрать слово
   */
  return (value: number): string => {
    if (value % 100 > 4 && value % 100 < 20) {
      return `${value} ${endings[2]}`;
    }

    const cases = [2, 0, 1, 1, 1, 2];
    const index = (value % 10 < 5) ? (value % 10) : 5;

    return `${value} ${endings[cases[index]]}`;
  };
}
