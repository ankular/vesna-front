import { ComponentType, lazy as ReactLazy } from 'react';

export function lazy<C extends ComponentType>(factory: () => Promise<C>) {
  return ReactLazy(() => factory().then((module) => ({ default: module })));
}
