import React, { ReactElement, ReactNode } from 'react';

export interface FormGroupProps {
  inline?: boolean;
  label?: string;
  children?: ReactNode;
}

export function FormGroup(props: FormGroupProps): ReactElement {
  return (
    <div className="form-group row">
      {props.label != null && (
        <label className={`col-12 col-form-label${props.inline ? ' col-md-3' : ''}`}>
          {props.label}
        </label>
      )}

      <div className={`col-12${props.inline ? ' col-md-9' : ''}`}>
        {props.children}
      </div>
    </div>
  );
}
