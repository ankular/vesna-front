import { Notification, NotificationGroup } from '@progress/kendo-react-notification';
import axios, { AxiosError, AxiosResponse } from 'axios';
import React, { Fragment, ReactElement, ReactNode, useEffect, useState } from 'react';

export interface AxiosProviderProps {
  children?: ReactNode;
}

const getStyleType = (status: number) => {
  if (status >= 400 && status <= 499) return 'warning';
  if (status >= 500 && status <= 599) return 'error';
};

const transformResponseData = (data: AxiosResponse['data']) => {
  if (typeof data === 'object' && data != null) {
    Object
      .entries(data)
      .forEach(([key, value]) => {
        if (/url$/i.test(key)) {
          return data[key] = [process.env.PROXY, value].join('/');
        }

        data[key] = transformResponseData(value);
      });
  }

  return data;
};

export function AxiosBoundary(props: AxiosProviderProps): ReactElement {
  const [errors, setErrors] = useState<Array<{
    message: string,
    styleType: 'none' | 'success' | 'error' | 'warning' | 'info',
  }>>([]);

  useEffect(() => {
    const interceptor = axios.interceptors.response.use(
      (response: AxiosResponse) => {
        transformResponseData(response.data);

        return response;
      },
      (error: AxiosError) => {
        if (error.response?.headers['content-type'].includes('application/json')) {
          if (error.response.data.hasOwnProperty('message')) {
            setErrors([ ...errors, {
              message: error.response.data.message,
              styleType: getStyleType(error.response.status),
            }]);
          }
        }

        return Promise.reject(error);
      },
    );

    return () => {
      axios.interceptors.response.eject(interceptor);
    };
  }, [errors.length]);

  return (
    <Fragment>
      {props.children}

      <NotificationGroup style={{ alignItems: 'flex-start', flexWrap: 'wrap-reverse', right: 10, top: 10 }}>
        {errors.map((error, index) => (
          <Notification
            key={index}
            closable
            type={{ style: error.styleType, icon: true }}
            onClose={() => [errors.splice(index, 1), setErrors([...errors])]}
            children={error.message}
          />
        ))}
      </NotificationGroup>
    </Fragment>
  );
}
