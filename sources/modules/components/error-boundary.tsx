import React, { Component, ErrorInfo, ReactElement, ReactNode } from 'react';

export interface ErrorBoundaryState {
  error?: Error;
  errorInfo?: ErrorInfo;
  hasError: boolean;
}

export interface ErrorBoundaryProps {
  fallback?: (error: Error, errorInfo: ErrorInfo) => ReactElement;
  children?: Readonly<{ children?: ReactNode }>;
}

export class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  public static getDerivedStateFromError() {
    return { hasError: true };
  }

  public state: ErrorBoundaryState = { hasError: false };

  public constructor(props: ErrorBoundaryProps) {
    super(props);
  }

  public componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    this.setState({ ...this.state, error, errorInfo });
  }

  public render() {
    if (this.state.hasError) {
      return this.props.fallback?.(this.state.error, this.state.errorInfo) ?? (
        <div className="m-2">
          <div>
            <div className="card">
              <div className="card-header">
                Что-то пошло не так
              </div>

              <details className="card-body" style={{ whiteSpace: 'pre-wrap' }}>
                {this.state.error?.name}
                {this.state.errorInfo?.componentStack}
              </details>

              <div className="d-flex card-footer justify-content-end">
                <div className="btn-group btn-group-sm">
                  <a className="btn btn-primary" href="/">
                    <i className="fa fa-home"/> На главную
                  </a>

                  <button className="btn btn-outline-primary" onClick={() => this.setState({ hasError: false })}>
                    <i className="fas fa-sync"/> Повторить
                  </button>

                  <a className="btn btn-outline-primary" href={window.location.href}>
                    <i className="fas fa-history"/> Обновить
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }

    return this.props.children;
  }
}
