import React, { ReactElement } from 'react';

export interface LoaderProps {
  height?: string | number;
}

export function Loader(props?: LoaderProps): ReactElement {
  return (<div className="loading" style={{ height: props?.height ?? '2rem' }}/>);
}
