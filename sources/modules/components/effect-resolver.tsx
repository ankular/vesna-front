import { DependencyList, Dispatch, ReactElement, SetStateAction, useEffect, useState } from 'react';

export enum PromiseStatus {
  Canceled = 'Canceled',
  Fulfilled = 'Fulfilled',
  Pending = 'Pending',
  Rejected = 'Rejected',
}

export interface EffectProps<S> {
  effect: () => Promise<S>;
  deps?: DependencyList;
  fallback: () => ReactElement;
  children: (data: S, setData?: Dispatch<SetStateAction<S>>) => ReactElement;
}

export function EffectResolver<S>(props: EffectProps<S>) {
  const [state, setState] = useState({ status: PromiseStatus.Pending, value: undefined });

  useEffect(() => {
    setState({ status: PromiseStatus.Pending, value: undefined });

    props.effect()
      .then((value) => {
        if (state.status !== PromiseStatus.Canceled) {
          setState({ status: PromiseStatus.Fulfilled, value });
        }
      })
      .catch((value) => {
        if (state.status !== PromiseStatus.Canceled) {
          setState({ status: PromiseStatus.Rejected, value });
        }
      });

    return () => {
      setState({ status: PromiseStatus.Canceled, value: undefined });
    };
  }, props.deps ?? []);

  if (state.status === PromiseStatus.Rejected) {
    throw Error(state.value);
  }

  if (state.status === PromiseStatus.Fulfilled) {
    return props.children(state.value);
  }

  return props.fallback();
}
