import { Employee } from 'main/employee/models';

export interface Holiday {
  description?: string;
  endAt?: string;
  startAt?: string;

  employeeId?: number;

  employee?: Employee;
}
