import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Holiday } from 'main/holiday/models';
import { holidayService } from 'main/holiday/services';
import { DatePicker } from '@progress/kendo-react-dateinputs';
import { DateTime } from 'luxon';
import { EmployeeComboBox } from 'main/employee/components';

export interface HolidayFormProps extends Omit<ResourceFormProps<Holiday>, 'service'> {
}

export function HolidayForm(props: HolidayFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={holidayService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Сотрудник">
            <EmployeeComboBox
              controls={false}
              disabled={submitted}
              required
              value={value.employee}
              onChange={(e) => setValue({ ...value, /*employee: e.value,*/ employeeId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Начало отпуска">
            <DatePicker
              className="w-100"
              disabled={submitted}
              format="dd.MM.yyyy"
              max={value.endAt ? DateTime.fromISO(value.endAt).toJSDate() : DateTime.local().toJSDate()}
              required
              value={value.startAt ? DateTime.fromISO(value.startAt).toJSDate() : undefined}
              onChange={(e) => setValue({ ...value, startAt: DateTime.fromJSDate(e.target.value).toISO() })}
            />
          </FormGroup>

          <FormGroup label="Конец отпуска">
            <DatePicker
              className="w-100"
              disabled={submitted}
              format="dd.MM.yyyy"
              min={value.startAt ? DateTime.fromISO(value.startAt).toJSDate() : undefined}
              required
              value={value.endAt ? DateTime.fromISO(value.endAt).toJSDate() : undefined}
              onChange={(e) => setValue({ ...value, endAt: DateTime.fromJSDate(e.target.value).toISO() })}
            />
          </FormGroup>

          <FormGroup label="Описание">
            <textarea
              className="k-textarea w-100"
              disabled={submitted}
              value={value.description ?? ''}
              onChange={(e) => setValue({ ...value, description: e.target.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
