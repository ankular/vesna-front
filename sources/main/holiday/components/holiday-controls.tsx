import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { HolidayForm } from 'main/holiday/components/holiday-form';
import { Holiday } from 'main/holiday/models';
import { holidayService } from 'main/holiday/services';

export interface HolidayControlsProps extends Omit<ResourceControlsProps<Holiday>, 'service'> {
}

export function HolidayControls(props: HolidayControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={HolidayForm}
      service={holidayService}
    />
  );
}
