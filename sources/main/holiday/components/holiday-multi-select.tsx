import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { HolidayControls } from 'main/holiday/components/holiday-controls';
import { HolidayItem } from 'main/holiday/components/holiday-item';
import { Holiday } from 'main/holiday/models';
import { holidayService } from 'main/holiday/services';

export interface HolidayMultiSelectProps extends Omit<ResourceMultiSelectProps<Holiday>, 'service'> {
}

export function HolidayMultiSelect(props: HolidayMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={HolidayControls}
      itemRender={HolidayItem}
      service={holidayService}
    />
  );
}
