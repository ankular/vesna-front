import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { Holiday } from 'main/holiday/models';
import { holidayService } from 'main/holiday/services';
import { DateTime } from 'luxon';

export interface HolidayItemProps extends Omit<ResourceItemProps<Holiday>, 'service'> {
}

export function HolidayItem(props: HolidayItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={holidayService}
      children={({ value }) => value && (
        <div>
          {value.employee.user.fullName + ', '}
          с {DateTime.fromISO(value.startAt).toFormat('dd.MM.yyyy') + ' '}
          по {DateTime.fromISO(value.endAt).toFormat('dd.MM.yyyy')}
        </div>
      )}
    />
  );
}
