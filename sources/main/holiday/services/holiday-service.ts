import { createResourceService } from 'modules/resource';

import { Holiday } from 'main/holiday/models';

const service = createResourceService<Holiday>({
  baseURL: '/api/holidays',
  dataItemKey: 'id',
  scope: 'employee(user())',
  textField: 'employee.user.fullName',
});

export const holidayService = Object.assign(service, {});
