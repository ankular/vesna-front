import { createResourceService } from 'modules/resource';

import { Service } from 'main/service/models';

const service = createResourceService<Service>({
  baseURL: '/api/services',
  dataItemKey: 'id',
  scope: 'serviceType(),employeeServices(employee(user()),service())',
  textField: 'name',
});

export const serviceService = Object.assign(service, {});
