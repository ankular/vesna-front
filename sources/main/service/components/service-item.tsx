import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { Service } from 'main/service/models';
import { serviceService } from 'main/service/services';

export interface ServiceItemProps extends Omit<ResourceItemProps<Service>, 'service'> {
}

export function ServiceItem(props: ServiceItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={serviceService}
    />
  );
}
