import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceControls } from 'main/service/components/service-controls';
import { ServiceItem } from 'main/service/components/service-item';
import { Service } from 'main/service/models';
import { serviceService } from 'main/service/services';

export interface ServiceMultiSelectProps extends Omit<ResourceMultiSelectProps<Service>, 'service'> {
}

export function ServiceMultiSelect(props: ServiceMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={ServiceControls}
      itemRender={ServiceItem}
      service={serviceService}
    />
  );
}
