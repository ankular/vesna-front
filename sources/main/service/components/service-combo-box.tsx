import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceControls } from 'main/service/components/service-controls';
import { ServiceItem } from 'main/service/components/service-item';
import { Service } from 'main/service/models';
import { serviceService } from 'main/service/services';

export interface ServiceComboBoxProps extends Omit<ResourceComboBoxProps<Service>, 'service'> {
}

export function ServiceComboBox(props: ServiceComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={ServiceControls}
      itemRender={ServiceItem}
      service={serviceService}
    />
  );
}
