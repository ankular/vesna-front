import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceForm } from 'main/service/components/service-form';
import { Service } from 'main/service/models';
import { serviceService } from 'main/service/services';

export interface ServiceControlsProps extends Omit<ResourceControlsProps<Service>, 'service'> {
}

export function ServiceControls(props: ServiceControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={ServiceForm}
      service={serviceService}
    />
  );
}
