import { Input, NumericTextBox } from '@progress/kendo-react-inputs';
import { ServiceTypeComboBox } from 'main/service-type/components';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Service } from 'main/service/models';
import { serviceService } from 'main/service/services';

export interface ServiceFormProps extends Omit<ResourceFormProps<Service>, 'service'> {
}

export function ServiceForm(props: ServiceFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={serviceService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Название">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.name ?? ''}
              onChange={(e) => setValue({ ...value, name: e.target.value })}
            />
          </FormGroup>

          <FormGroup label="Тип услуги">
            <ServiceTypeComboBox
              disabled={submitted}
              required
              value={value.serviceType}
              onChange={(e) => setValue({ ...value, /*serviceType: e.value,*/ serviceTypeId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Цена">
            <NumericTextBox
              className="w-100"
              disabled={submitted}
              value={value.price}
              onChange={(e) => setValue({ ...value, price: e.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
