import { Attachment } from 'main/attachment/models';
import { Employee } from 'main/employee/models';
import { Order } from 'main/order/models';
import { ServiceType } from 'main/service-type/models';

export interface Service {
  id?: number;

  description?: string;
  name?: string;
  price?: number;

  imageId?: number;
  serviceTypeId?: number;

  readonly imageUrl?: string;

  employees?: Employee[];
  image?: Attachment;
  orders?: Order[];
  serviceType?: ServiceType;
}
