import { createSlice } from 'crunch-store';

import { LocalUser } from 'main/core/models';

export interface AuthState {
  currentUser: LocalUser;
}

export const authStore = createSlice({
  state: {
    currentUser: undefined,
  } as AuthState,
  getters(state) {
    return {
      getIsAuthenticated() {
        return !!state.currentUser;
      },
    };
  },
  reducers(state) {
    return {
      setCurrentUser(user: LocalUser) {
        return { ...state, currentUser: user };
      },
    };
  },
});
