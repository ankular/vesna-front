import axios from 'axios';

import { LocalUser } from 'main/core/models';

export const authService = {
  check(): Promise<LocalUser> {
    return axios
      .get<LocalUser>('/api/auth/check')
      .then(({ data }) => data)
      .catch(() => undefined);
  },
  signIn(user: LocalUser): Promise<LocalUser> {
    return axios
      .put<LocalUser>('/api/auth/sign-in', user)
      .then(({ data }) => data)
      .catch(() => undefined);
  },
  signOut(): Promise<LocalUser> {
    return axios
      .delete<LocalUser>('/api/auth/sign-out')
      .then(() => undefined)
      .catch(() => undefined);
  },
  signUp(user: LocalUser): Promise<LocalUser> {
    return axios
      .post<LocalUser>('/api/auth/sign-up', user)
      .then(({ data }) => data)
      .catch(() => undefined);
  },
};
