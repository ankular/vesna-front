import { AttachmentGallery } from 'main/attachment/components';
import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';
import { ServiceType } from 'main/service-type/models';
import { serviceTypeService } from 'main/service-type/services';
import React, { ReactElement, useEffect, useState } from 'react';

export function MainGalleryPage(): ReactElement {
  const [serviceTypes, setServiceTypes] = useState<ServiceType[]>([]);
  const [selectedServiceType, setSelectedServiceType] = useState<ServiceType>();
  const [attachmentsByCategory, setAttachmentsByCategory] = useState<Attachment[]>();
  const [attachments, setAttachments] = useState<Attachment[]>([]);

  useEffect(() => {
    serviceTypeService
      .query({ scope: serviceTypeService.options.scope })
      .then((result) => setServiceTypes(result.data));
  }, []);

  useEffect(() => {
    if (attachments.length > 0) {
      if (selectedServiceType?.id) {
        const x = attachments.filter((attachment) => {
          return attachment.examples
            .filter((example) => example.service.serviceTypeId === selectedServiceType?.id)
            .length > 0;
        });

        setAttachmentsByCategory(x);
      } else {
        setAttachmentsByCategory(attachments.filter((attachment) => attachment.examples.length > 0));
      }
    }
  }, [selectedServiceType?.id, attachments.length]);

  useEffect(() => {
    attachmentService
      .query({ 'pager.take': 100000, 'scope': attachmentService.options.scope } as any)
      .then((result) => setAttachments(result.data));
  }, []);

  return (
    <>
      <h3 className="text-center">Галерея</h3>

      <ul className="nav">
        <li className="nav-item">
          <a
            className={`nav-link${selectedServiceType == null ? ' text-dark' : ' text-primary'}`}
            style={{ cursor: 'pointer' }}
            onClick={() => setSelectedServiceType(undefined)}
          >
            Все
          </a>
        </li>
        {serviceTypes.map((serviceType, index) => (
          <li key={index} className="nav-item">
            <a
              className={`nav-link${selectedServiceType?.id === serviceType.id ? ' text-dark' : ' text-primary'}`}
              style={{ cursor: 'pointer' }}
              onClick={() => setSelectedServiceType(serviceType)}
            >
              {serviceType.name}
            </a>
          </li>
        ))}
      </ul>

      <AttachmentGallery
        data={attachmentsByCategory}
        children={(attachment: Attachment) => (
          <div className="text-center">
            <div>Мастер: {attachment.examples[0].employee.user.fullName}</div>
            <div>Услуга: {attachment.examples[0].service.name}</div>
          </div>
        )}
      />
    </>
  );
}
