import React, { ReactElement } from 'react';

export interface MainContactPageProps {
  contactInfo: {
    phone: string;
    work: string;
    address: string;
  };
}

export function MainContactPage(props: MainContactPageProps): ReactElement {
  return (
    <div>
      <h3 className="text-center">Контакты</h3>

      <ul className="list-unstyled">
        <li>{props.contactInfo.phone}</li>
        <li>{props.contactInfo.work}</li>
        <li>{props.contactInfo.address}</li>
      </ul>

      <iframe
        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1992.0101930043818!2d30.316855616144384!3d60.04805926022893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x469634e8f5052a2d%3A0x565095c46ab87efd!2z0YPQuy4g0JrQvtC80L_QvtC30LjRgtC-0YDQvtCyLCA1LCDQodCw0L3QutGCLdCf0LXRgtC10YDQsdGD0YDQsywgMTk0MzU2!5e0!3m2!1sru!2sru!4v1588248441228!5m2!1sru!2sru"
        width="100%"
        height="450"
        frameBorder="0"
        style={{ border: 0 }}
        allowFullScreen
      />
    </div>
  );
}
