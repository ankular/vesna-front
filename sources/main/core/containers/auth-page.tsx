import React, { ReactElement } from 'react';

import { AuthForm } from 'main/core/components';

export function AuthPage(): ReactElement {
  return (
    <section className="container">
      <div className="justify-content-center row">
        <div className="col-12 col-md-6 col-sm-10 mt-3 mt-md-5">
          <AuthForm/>
        </div>
      </div>
    </section>
  );
}
