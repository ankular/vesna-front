import { ServiceType } from 'main/service-type/models';
import { serviceTypeService } from 'main/service-type/services';
import { Service } from 'main/service/models';
import React, { Fragment, ReactElement, useEffect, useState } from 'react';

export function MainServicesPage(): ReactElement {
  const [serviceTypes, setServiceTypes] = useState<ServiceType[]>([]);
  const [selectedServiceType, setSelectedServiceType] = useState<ServiceType>();
  const [allServices, setAllServices] = useState<Service[]>([]);

  useEffect(() => {
    serviceTypeService
      .query({ scope: serviceTypeService.options.scope })
      .then((result) => {
        setAllServices(result.data.flatMap(({ services }) => services));
        setServiceTypes(result.data);
      });
  }, []);

  return (
    <Fragment>
      <h3 className="text-center">Услуги</h3>

      <ul className="nav">
        <li className="nav-item">
          <a
            className={`nav-link${selectedServiceType == null ? ' text-dark' : ' text-primary'}`}
            style={{ cursor: 'pointer' }}
            onClick={() => setSelectedServiceType(undefined)}
          >
            Все
          </a>
        </li>
        {serviceTypes.map((serviceType, index) => (
          <li key={index} className="nav-item">
            <a
              className={`nav-link${selectedServiceType?.id === serviceType.id ? ' text-dark' : ' text-primary'}`}
              style={{ cursor: 'pointer' }}
              onClick={() => setSelectedServiceType(serviceType)}
            >
              {serviceType.name}
            </a>
          </li>
        ))}
      </ul>

      <table className="table table-striped">
        <thead className="bg-light">
        <tr>
          <th className="border-0">Название услуги</th>
          <th className="border-0">Цена</th>
        </tr>
        </thead>
        <tbody>
        {!selectedServiceType && allServices.map((service, index) => (
          <tr key={index}>
            <td>{service.name} ({service.serviceType.name})</td>
            <td>{service.price} <i className="fas fa-ruble-sign"/></td>
          </tr>
        ))}

        {selectedServiceType?.services.map((service, index) => (
          <tr key={index}>
            <td>{service.name}</td>
            <td>{service.price}<i className="fas fa-rub"/></td>
          </tr>
        ))}
        </tbody>
      </table>
    </Fragment>
  );
}
