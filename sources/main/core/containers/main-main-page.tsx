import { Employee } from 'main/employee/models';
import { employeeService } from 'main/employee/services';
import React, { ReactElement, useEffect, useState } from 'react';

export function MainMainPage(): ReactElement {
  const [employees, setEmployees] = useState<Employee[]>([]);

  useEffect(() => {
    employeeService
      .query({ scope: employeeService.options.scope })
      .then((result) => setEmployees(result.data));
  }, []);

  return (
    <div className="py-2">
      <img src="images/main-image.png" className="img-fluid"/>

      {/* tslint:disable-next-line:max-line-length */}
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tristique senectus et netus et malesuada fames ac turpis egestas. Morbi tristique senectus et netus et malesuada fames ac. Arcu ac tortor dignissim convallis. Egestas dui id ornare arcu odio ut. Amet massa vitae tortor condimentum lacinia quis. Facilisis sed odio morbi quis commodo odio aenean. Consectetur lorem donec massa sapien faucibus. Ipsum nunc aliquet bibendum enim facilisis gravida neque convallis. Justo donec enim diam vulputate. Fusce ut placerat orci nulla. Netus et malesuada fames ac turpis egestas. Phasellus vestibulum lorem sed risus. Varius morbi enim nunc faucibus a pellentesque sit amet. Donec massa sapien faucibus et molestie. In hac habitasse platea dictumst vestibulum rhoncus est pellentesque. Nibh tellus molestie nunc non blandit. Egestas maecenas pharetra convallis posuere morbi leo urna molestie at. Massa tincidunt dui ut ornare lectus sit amet est placerat. Urna porttitor rhoncus dolor purus non.</p>

      <h3 className="text-center">Наши мастера</h3>

      <div className="card-columns">
        {employees.map((employee, index) => (
          <div key={index} className="card">
            <img alt="" className="card-img-top" src={employee.user.avatarUrl}/>
            <div className="card-body">
              <div>ФИО: {employee.user.fullName}</div>
              <div>Должность: {employee.positions.map((position) => position.name).join(', ')}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
