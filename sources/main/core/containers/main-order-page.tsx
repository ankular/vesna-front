import { Dialog } from '@progress/kendo-react-dialogs';
import { OrderForm } from 'main/order/components';
import React, { ReactElement, useState } from 'react';
import { useHistory } from 'react-router';

export function MainOrderPage(): ReactElement {
  const [isSuccessDialogShow, setIsSuccessDialogShow] = useState<boolean>(false);
  const history = useHistory();

  return (
    <div>
      <h3 className="text-center">Запишитесь к нам</h3>
      <OrderForm id={0} step={0} onSave={() => setIsSuccessDialogShow(true)} isAdmin={false}/>

      {isSuccessDialogShow && (
        <Dialog title="Вы записаны" onClose={() => {
          setIsSuccessDialogShow(true);
          history.push('/main');
        }}>
          <div>Ожидайте подтверждение от администратора</div>
        </Dialog>
      )}
    </div>
  );
}
