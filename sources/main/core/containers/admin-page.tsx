import { useStoreSetter, useStoreState } from 'crunch-store';
import { Loader } from 'modules/components';
import { useBreadcrumbs, useMainRoutes } from 'modules/hooks';
import React, { Fragment, ReactElement, Suspense, useState } from 'react';
import { NavLink, Route, Switch } from 'react-router-dom';

import { authService } from 'main/core/services';
import { routes } from 'main/routes';
import { RootStore } from 'main/store';

export function AdminPage(): ReactElement {
  const { currentUser } = useStoreState<RootStore>().authStore;
  const { setCurrentUser } = useStoreSetter<RootStore>().authStore;
  const [isCollapseVisible, setIsCollapseVisible] = useState(false);
  const breadcrumbs = useBreadcrumbs(routes);
  const mainRoutes = useMainRoutes(routes);

  const handleSignOut = () => {
    authService
      .signOut()
      .then(setCurrentUser);
  };

  return (
    <Fragment>
      <nav className="bg-dark navbar navbar-expand-md navbar-dark pb-1">
        <div className="container flex-wrap">
          <div className="d-flex px-2 mb-1 w-100">
            <NavLink className="navbar-brand" to="/admin">Весна</NavLink>

            <div className="btn-group ml-auto mr-2">
              {/*<NavLink className="btn btn-link nav-link" to={`/users/${currentUser?.userId}`}>*/}
              {/*  <i className="fas fa-user"/>*/}
              {/*</NavLink>*/}

              <button className="btn btn-link nav-link" onClick={handleSignOut}>
                <i className="fas fa-power-off"/>
              </button>
            </div>

            <button className="navbar-toggler" onClick={() => setIsCollapseVisible(!isCollapseVisible)}>
              <i className="fas fa-bars"/>
            </button>
          </div>

          <div className={`border-light border-top collapse navbar-collapse w-100${isCollapseVisible ? ' show' : ''}`}>
            <ul className="d-flex flex-wrap navbar-nav mr-auto">
              {mainRoutes.map((route, index) => (
                <li key={index} className="nav-item">
                  <NavLink className="nav-link px-2" to={route.path} onClick={() => setIsCollapseVisible(false)}>
                    {route.title}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        {!!breadcrumbs.length && (
          <nav aria-label="breadcrumb">
            <ol className="breadcrumb bg-secondary rounded-0">
              {breadcrumbs.map((breadcrumb, index) => (
                <li key={index} className="breadcrumb-item">
                  <NavLink className="text-white" to={breadcrumb.url}>{breadcrumb.title}</NavLink>
                </li>
              ))}
            </ol>
          </nav>
        )}

        <Suspense fallback={<Loader/>}>
          <Switch>
            {routes.map((route, index) => (
              <Route key={index} component={route.component} exact path={route.path}/>
            ))}
          </Switch>
        </Suspense>
      </div>
    </Fragment>
  );
}
