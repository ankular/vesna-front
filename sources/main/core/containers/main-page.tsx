import { MainContactPage } from 'main/core/containers/main-contact-page';
import { MainGalleryPage } from 'main/core/containers/main-gallery-page';
import { MainMainPage } from 'main/core/containers/main-main-page';
import { MainOrderPage } from 'main/core/containers/main-order-page';
import { MainServicesPage } from 'main/core/containers/main-services-pages';
import { Loader } from 'modules/components';
import React, { Fragment, ReactElement, Suspense, useState } from 'react';
import { NavLink, Redirect, Route, Switch } from 'react-router-dom';

const contactInfo = {
  address: 'ул. Композиторов, д. 5',
  phone: '+7 (999) 999-99-99',
  work: 'пн-сб 09:00-21:00',
};

const mainPageRoutes = {
  left: [
    { path: '/main', title: 'Главная', component: MainMainPage },
    { path: '/gallery', title: 'Галерея', component: MainGalleryPage },
    { path: '/services', title: 'Услуги', component: MainServicesPage },
    { path: '/contacts', title: 'Контакты', component: () => MainContactPage({ contactInfo }) },
  ],
  right: [
    { path: '/order', title: 'Запишитесь к нам', component: MainOrderPage },
  ],
};

export function MainPage(): ReactElement {
  const [isCollapseVisible, setIsCollapseVisible] = useState(false);

  return (
    <Fragment>
      <nav className="bg-success navbar navbar-expand-md navbar-light pb-1">
        <div className="container flex-wrap">
          <div className="d-flex px-2 mb-1 w-100">
            <NavLink className="navbar-brand" to="/">Весна</NavLink>

            <div className="btn-group ml-auto mr-2">
              <a className="btn btn-link nav-link text-dark px-0" href={`tel:${contactInfo.phone}`}>{contactInfo.phone}</a>
            </div>

            <button className="navbar-toggler" onClick={() => setIsCollapseVisible(!isCollapseVisible)}>
              <i className="fas fa-bars"/>
            </button>
          </div>

          <div className={`collapse navbar-collapse w-100${isCollapseVisible ? ' show' : ''}`}>
            <ul className="d-flex flex-wrap navbar-nav mr-auto">
              {mainPageRoutes.left.map((mainPageRoute, index) => (
                <li key={index} className="nav-item">
                  <NavLink className="nav-link px-2" to={mainPageRoute.path} onClick={() => setIsCollapseVisible(false)}>
                    {mainPageRoute.title}
                  </NavLink>
                </li>
              ))}
            </ul>

            <ul className="d-flex flex-wrap navbar-nav ml-auto mr-2">
              {mainPageRoutes.right.map((mainPageRoute, index) => (
                <li key={index} className="nav-item">
                  <NavLink className="nav-link px-2" to={mainPageRoute.path} onClick={() => setIsCollapseVisible(false)}>
                    {mainPageRoute.title}
                  </NavLink>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>

      <div className="container bg-white mt-3">
        <Suspense fallback={<Loader/>}>
          <Switch>
            {mainPageRoutes.left.map((route, index) => (
              <Route key={index} component={route.component} exact path={route.path}/>
            ))}

            {mainPageRoutes.right.map((route, index) => (
              <Route key={index} component={route.component} exact path={route.path}/>
            ))}

            <Route exact path="/">
              <Redirect to="/main"/>
            </Route>
          </Switch>
        </Suspense>
      </div>
    </Fragment>
  );
}
