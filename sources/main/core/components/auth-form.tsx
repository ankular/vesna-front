import { Input } from '@progress/kendo-react-inputs';
import { useStoreSetter } from 'crunch-store';
import React, { ReactElement, SyntheticEvent, useState } from 'react';

import { LocalUser } from 'main/core/models';
import { authService } from 'main/core/services';
import { RootStore } from 'main/store';

export function AuthForm(): ReactElement {
  const [form, setForm] = useState<LocalUser>({});
  const [submitted, setSubmitted] = useState<boolean>();
  const { setCurrentUser } = useStoreSetter<RootStore>().authStore;

  const handleSingIn = (event: SyntheticEvent) => {
    event.preventDefault();
    setSubmitted(true);

    authService
      .signIn(form)
      .then(setCurrentUser)
      .finally(() => setSubmitted(false));
  };

  const handleSingUp = (event: SyntheticEvent) => {
    event.preventDefault();
    setSubmitted(true);

    authService
      .signUp(form)
      .then(setCurrentUser)
      .finally(() => setSubmitted(false));
  };

  return (
    <form onSubmit={handleSingIn}>
      <div className="form-group">
        <Input
          className="w-100"
          disabled={submitted}
          placeholder="Имя пользователя"
          required
          value={form.username ?? ''}
          onChange={(e) => setForm({ ...form, username: e.target.value })}
        />
      </div>

      <div className="form-group">
        <Input
          className="w-100"
          disabled={submitted}
          placeholder="Пароль"
          required
          type="password"
          value={form.password ?? ''}
          onChange={(e) => setForm({ ...form, password: e.target.value })}
        />
      </div>

      <div className="btn-group w-100">
        <button className="btn btn-primary w-50" disabled={submitted} type="submit">
          <i className="fas fa-door-open"/> Войти
        </button>

        <button className="btn btn-secondary w-50" disabled={submitted} type="button" onClick={handleSingUp}>
          <i className="fas fa-user-plus"/> Регистрация
        </button>
      </div>
    </form>
  );
}
