import { LocalUser } from 'main/core/models/local-user';

export interface Session {
  id?: number;

  cookie?: string;

  localUserId?: string;

  localUser?: LocalUser;
}
