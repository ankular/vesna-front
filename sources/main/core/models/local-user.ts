import { Session } from 'main/core/models/session';
import { Role } from 'main/role/models';
import { User } from 'main/user/models';

export interface LocalUser {
  id?: number;

  password?: string;
  username?: string;

  userId?: number;

  user?: User;
  roles?: Role[];
  sessions?: Session[];
}
