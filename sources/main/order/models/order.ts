import { Client } from 'main/client/models';
import { EmployeeService } from 'main/employee/models';

export interface Order {
  client: Client;
  clientId: number;
  orderEmployeeServices: Array<{
    executionAt;
    employeeService: EmployeeService;
  }>;
}
