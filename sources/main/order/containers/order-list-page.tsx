import { Day } from '@progress/kendo-date-math';
import { IntlProvider, LocalizationProvider, load } from '@progress/kendo-react-intl';
import { SchedulerItemProps } from '@progress/kendo-react-scheduler/dist/npm/items/SchedulerItem';
import { SchedulerViewItemProps, SchedulerViewItem } from '@progress/kendo-react-scheduler/dist/npm/items/SchedulerViewItem';
import { DataAction, SchedulerDataChangeEvent } from '@progress/kendo-react-scheduler/dist/npm/Scheduler';
import { DateTime } from 'luxon';
import { Dialog } from '@progress/kendo-react-dialogs';

import {
  AgendaView,
  DayView,
  MonthView,
  Scheduler,
  SchedulerItem,
  TimelineView,
  WeekView,
} from '@progress/kendo-react-scheduler';
import { OrderForm } from 'main/order/components';
import { Order } from 'main/order/models';

import { orderService } from 'main/order/services';
import React, { ReactElement, useEffect, useState } from 'react';
import { displayDate, sampleData } from './data';


import likelySubtags from 'cldr-core/supplemental/likelySubtags.json';
import currencyData from 'cldr-core/supplemental/currencyData.json';
import weekData from 'cldr-core/supplemental/weekData.json';

import numbers from 'cldr-numbers-full/main/ru/numbers.json';
import currencies from 'cldr-numbers-full/main/ru/currencies.json';
import caGregorian from 'cldr-dates-full/main/ru/ca-gregorian.json';
import dateFields from 'cldr-dates-full/main/ru/dateFields.json';
import timeZoneNames from 'cldr-dates-full/main/ru/timeZoneNames.json';

load(
  likelySubtags,
  currencyData,
  weekData,
  numbers,
  currencies,
  caGregorian,
  dateFields,
  timeZoneNames,
);

export function OrderListPage(): React.ReactElement {
  const [orders, setOrders] = useState<Order[]>([]);
  const [selectedOrderId, setSelectedOrderId] = useState<number>();
  const [updateIndex, setUpdateIndex] = useState(0);
  const [data, setData] = useState<any>();

  useEffect(() => {
    orderService
      .query({ 'pager.take': 1000000, 'scope': orderService.options.scope } as any)
      .then((result) => {
        const getShortName = (name) => {
          const [lastName, firstName, secondName] = name.split(' ');
          return `${lastName} ${firstName && firstName[0] + '.' || ''} ${secondName && secondName[0] + '.' || ''}`;
        };

        const x = result.data.filter((order) => order['deletedAt'] == null).map((order: any) => {
          order.title = order.orderEmployeeServices.map(({ employeeService }) => {
            return `
              ${getShortName(employeeService.employee.user.fullName)} - ${employeeService.service.name}
              [${getShortName(order.client.user.fullName)}]
            `;
          }).join(', ');
          order.orderEmployeeServices.sort((a, b) => new Date(a.executionAt).getTime() - new Date(b.executionAt).getTime());
          order.start = new Date(order.orderEmployeeServices[0]?.executionAt);

          const leadTimes = order.orderEmployeeServices.map(({ employeeService }) => employeeService.leadTime);
          const leadTimeMinutes = leadTimes.reduce((acc, it) => {
            const [hour, minute] = it.split(':');
            return acc + +hour * 60 + +minute;
          }, 0);

          order.end = DateTime.fromJSDate(order.start).plus({ minute: leadTimeMinutes }).toJSDate();
          return order;
        });

        setData(x);
        setOrders(result.data);
      });
  }, [updateIndex]);

  const handleOrderSave = () => {
    setSelectedOrderId(undefined);
    setUpdateIndex(updateIndex + 1);
  };

  const CustomItem = (props: SchedulerItemProps) => {
    const handleClick = () => {
      setSelectedOrderId(props.dataItem.id);
    };

    return (
      <SchedulerItem
        {...props}
        onClick={handleClick}
        style={{ ...props.style, backgroundColor: props.isAllDay ? 'pink' : 'blue' }}
      />
    );
  };

  return (
    <>
      <button type="button" className="btn btn-primary" onClick={() => setSelectedOrderId(0)}>Новый заказ</button>

      <LocalizationProvider language="ru-RU">
        <IntlProvider locale="ru">
          <Scheduler
            data={data}
            item={CustomItem}
          >
            <AgendaView title="Кратко"/>
            <DayView title="День"/>
            <WeekView title="Неделя"/>
            <MonthView title="Месяц"/>
          </Scheduler>
        </IntlProvider>
      </LocalizationProvider>

      {selectedOrderId != null && (
        <Dialog>
          <OrderForm id={selectedOrderId} step={0} onSave={handleOrderSave} isAdmin/>
        </Dialog>
      )}
    </>
  );
}
