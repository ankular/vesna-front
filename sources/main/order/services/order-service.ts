import { createResourceService } from 'modules/resource';

import { Order } from 'main/order/models';

const service = createResourceService<Order>({
  baseURL: '/api/orders',
  dataItemKey: 'id',
  scope: `
    client(
      user()
    ),
    orderEmployeeServices(
      employeeService(
        employee(
          user()
        ),
        service(
          serviceType()
        )
      )
    )`.replace(/\n\r|\n|\r|\s/g, ''),
  textField: '',
});

export const orderService = Object.assign(service, {});
