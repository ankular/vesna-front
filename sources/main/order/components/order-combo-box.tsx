import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { OrderItem } from 'main/order/components/order-item';
import { Order } from 'main/order/models';
import { orderService } from 'main/order/services';

export interface OrderComboBoxProps extends Omit<ResourceComboBoxProps<Order>, 'service'> {
}

export function OrderComboBox(props: OrderComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      itemRender={OrderItem}
      service={orderService}
    />
  );
}
