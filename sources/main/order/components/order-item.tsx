import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Order } from 'main/order/models';
import { orderService } from 'main/order/services';

export interface OrderItemProps extends Omit<ResourceItemProps<Order>, 'service'> {
}

export function OrderItem(props: OrderItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      extract
      service={orderService}
      children={({ value }) => value && (
        <div className="d-flex flex-column w-100">
          {/*<div className="d-flex justify-content-between">*/}
          {/*  <h5 className="mb-1">{value.user.fullName}</h5>*/}
          {/*  <small>{value.id}</small>*/}
          {/*</div>*/}

          {/*{value.positions?.length > 0 && (*/}
          {/*  <div className="mb-1">*/}
          {/*    <b>Должности: </b>*/}
          {/*    {value.positions.map((position, index, positions) => (*/}
          {/*      <span key={index}>{position.name}{positions.length - 1 > index && (', ')}</span>*/}
          {/*    ))}*/}
          {/*  </div>*/}
          {/*)}*/}

          {/*{value.services?.length > 0 && (*/}
          {/*  <div className="mb-1">*/}
          {/*    <b>Услуги: </b>*/}
          {/*    {value.services.map((service, index, services) => (*/}
          {/*      <Fragment key={index}>*/}
          {/*        {service.name}*/}
          {/*        <small> ({service.serviceType.name})</small>*/}
          {/*        {services.length - 1 > index && (', ')}*/}
          {/*      </Fragment>*/}
          {/*    ))}*/}
          {/*  </div>*/}
          {/*)}*/}
        </div>
      )}
    />
  );
}
