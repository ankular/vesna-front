import { DateTimePicker } from '@progress/kendo-react-dateinputs';
import { Dialog } from '@progress/kendo-react-dialogs';
import { Checkbox, Input, MaskedTextBox } from '@progress/kendo-react-inputs';
import axios from 'axios';
import { DateTime } from 'luxon';
import { ClientComboBox } from 'main/client/components';
import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';
import { EmployeeComboBox } from 'main/employee/components';
import { Order } from 'main/order/models';
import { orderService } from 'main/order/services';
import { ServiceComboBox } from 'main/service/components/service-combo-box';
import { UserForm } from 'main/user/components';
import { User } from 'main/user/models';
import { userService } from 'main/user/services';
import { FormGroup } from 'modules/components';
import React, { ReactElement, SyntheticEvent, useCallback, useEffect, useState } from 'react';

export interface OrderFormProps {
  id;
  step: number;
  onSave;
  isAdmin?: boolean;
}

export function OrderForm(props: OrderFormProps): ReactElement {
  const [order, setOrder] = useState<Order>();
  const [orderEmployeeService, setOrderEmployeeService] = useState<any>();
  const [orderEmployeeServiceStep, setOrderEmployeeServiceStep] = useState(props.step);
  const [user, setUser] = useState<User>({});
  const [isCustomUser, setIsCustomUser] = useState<boolean>(!props.isAdmin);
  const [isErrorDialogShown, setIsErrorDialogShown] = useState<boolean>(false);
  const [orderSum, setOrderSum] = useState<number>(0);

  useEffect(() => {
    if (props.id > 0) {
      orderService
        .get(props.id, { scope: orderService.options.scope })
        .then(setOrder);
    } else {
      setOrder({ orderEmployeeServices: [] } as any);
    }
  }, []);

  useEffect(() => {
    if (orderEmployeeServiceStep === 3) {
      const { employeeId, serviceId } = orderEmployeeService.employeeService;
      const params = { 'where.eq-employeeId': employeeId, 'where.eq-serviceId': serviceId };

      axios
        .get('/api/employee-services', { params })
        .then(({ data: { data } }: any) => {
          (order.orderEmployeeServices as any).push({
            employeeService: {
              ...data[0],
              employee: orderEmployeeService.employeeService.employee,
              service: orderEmployeeService.employeeService.service,
            },
            employeeServiceId: data[0].id,
            executionAt: orderEmployeeService.executionAt,
          });
          setOrder({ ...order });
          setOrderEmployeeServiceStep(0);
          setOrderEmployeeService(undefined);
        });
    }
  }, [orderEmployeeServiceStep === 3]);

  useEffect(() => {
    if (order?.orderEmployeeServices?.length != null) {
      setOrderSum(order.orderEmployeeServices.reduce((acc, value) => acc + value.employeeService.service.price, 0));
    }
  }, [order?.orderEmployeeServices?.length]);

  const handleFormSubmit = async (event: SyntheticEvent) => {
    event.preventDefault();

    if (order.orderEmployeeServices.length === 0) {
      return setIsErrorDialogShown(true);
    }

    if (isCustomUser) {
      const result1 = await userService.create(user);
      const result2 = await clientService.create({ user: result1, userId: result1.id });
      order.client = result2;
      order.clientId = result2.id;
    }
    setOrderEmployeeService(undefined);
    setOrderEmployeeServiceStep(0);

    if (props.id > 0) orderService.update(order).then(props.onSave);
    else orderService.create(order).then(props.onSave);
  };

  const handleRemoveOrderEmployeeService = (index) => {
    order.orderEmployeeServices[index]['deleted'] = true;
    setOrder({ ...order });
  };

  const handleOrderRemove = () => {
    orderService.remove(order).then(props.onSave);
  };

  if (order == null) {
    return (<div>Загрузка</div>);
  }

  return (
    <>
      {isErrorDialogShown && (
        <Dialog title="Ошибка" onClose={() => setIsErrorDialogShown(false)}>
          Необходимо выбрать хотя бы одну услугу
        </Dialog>
      )}

      <form onSubmit={handleFormSubmit}>
        {props.isAdmin && (
          <Checkbox value={isCustomUser} onChange={(e) => setIsCustomUser(e.value)} label="Новый клиент"/>
        )}

        {!isCustomUser && (<FormGroup label="Клиент">
          <ClientComboBox
            controls={false}
            value={order.client}
            onChange={(e) => setOrder({ ...order, client: e.value, clientId: e.value.id })}
          />
        </FormGroup>)}

        {isCustomUser && (
          <>
            <FormGroup label="ФИО">
              <Input
                className="w-100"
                required
                value={user.fullName ?? ''}
                onChange={(e) => setUser({ ...user, fullName: e.target.value })}
              />
            </FormGroup>

            <FormGroup label="Телефон">
              <MaskedTextBox
                className="w-100"
                mask="+7 (000) 000-00-00"
                placeholder="+7 (000) 000-00-00"
                required
                value={user.phone ?? ''}
                onChange={(e) => setUser({ ...user, phone: e.target.rawValue.trim() ? e.target.value : undefined })}
              />
            </FormGroup>
          </>
        )}

        <table className="table table-sm">
          <thead>
          <tr>
            <th>Позиция</th>
            <th>Мастер</th>
            <th>Услуга</th>
            <th>Цена</th>
            <th>Дата и время</th>
            <th>Действия</th>
          </tr>
          </thead>

          <tbody>
          {order.orderEmployeeServices.filter(({ deleted }: any) => !deleted).map((_, i1) => (
            <tr key={i1}>
              <td>{i1 + 1}</td>
              <td>{order.orderEmployeeServices[i1].employeeService.employee.user.fullName}</td>
              <td>{order.orderEmployeeServices[i1].employeeService.service.name}</td>
              <td>{order.orderEmployeeServices[i1].employeeService.service.price} <i className="fas fa-ruble-sign"/></td>

              <td>
                {new Date(order.orderEmployeeServices[i1].executionAt).toLocaleString()}
              </td>

              <td>
                <button className="btn btn-primary" type="button" onClick={() => handleRemoveOrderEmployeeService(i1)}>
                  <i className="fa fa-trash"/>
                </button>
              </td>
            </tr>
          ))}

          <tr>
            <td>{order.orderEmployeeServices.length + 1}</td>
            <td>
              {orderEmployeeServiceStep === 0 && (
                <EmployeeComboBox
                  controls={false}
                  onChange={(e) => {
                    setOrderEmployeeService({ employeeService: { employee: e.value, employeeId: e.value.id } });
                    setOrderEmployeeServiceStep(1);
                  }}
                />
              )}

              {orderEmployeeServiceStep > 0 && (<>{orderEmployeeService.employeeService.employee.user.fullName}</>)}
            </td>

            <td>
              {orderEmployeeServiceStep === 1 && (
                <ServiceComboBox
                  controls={false}
                  params={{ 'where.eq-employees.id': orderEmployeeService.employeeService.employeeId }}
                  onChange={(e) => {
                    orderEmployeeService.employeeService.service = e.value;
                    orderEmployeeService.employeeService.serviceId = e.value.id;
                    setOrderEmployeeService({ ...orderEmployeeService });

                    if (order.orderEmployeeServices.length === 0) {
                      setOrderEmployeeServiceStep(2);
                    } else {
                      const lastExecutionAt = order.orderEmployeeServices[order.orderEmployeeServices.length - 1].executionAt;
                      const [hour, minute] = order.orderEmployeeServices[order.orderEmployeeServices.length - 1].employeeService.leadTime.split(':');

                      orderEmployeeService.executionAt = DateTime.fromJSDate(lastExecutionAt).plus({ hour: +hour, minute: +minute }).toISO();
                      // console.error('hour, minute', hour, minute, lastExecutionAt, DateTime.fromISO(lastExecutionAt).toISO());
                      setOrderEmployeeService({ ...orderEmployeeService });
                      setOrderEmployeeServiceStep(3);
                    }
                  }}
                />
              )}

              {orderEmployeeServiceStep > 1 && (<>{orderEmployeeService.employeeService.service.name}</>)}
            </td>

            <td>
              {orderEmployeeServiceStep > 1 && (<>{orderEmployeeService.employeeService.service.price}</>)}
            </td>

            <td>
              {orderEmployeeServiceStep === 2 && (
                <DateTimePicker
                  format="yyyy-MM-dd hh:mm"
                  required
                  onChange={(e) => {
                    orderEmployeeService.executionAt = e.value;
                    setOrderEmployeeService({ ...orderEmployeeService });
                    setOrderEmployeeServiceStep(3);
                  }}
                />
              )}
            </td>

            <td>
              <button className="btn btn-primary" type="button" onClick={() => {
                setOrderEmployeeService(undefined);
                setOrderEmployeeServiceStep(0);
              }}>
                <i className="fas fa-undo"/>
              </button>
            </td>
          </tr>

          <tr>
            <td colSpan={5}/>
            <td>Сумма: {orderSum} <i className="fas fa-ruble-sign"/></td>
          </tr>
          </tbody>
        </table>

        {!props.isAdmin && (
          <div className="text-center">
            <button className="btn btn-lg btn-primary w-50 m-3">Записаться</button>
          </div>
        )}

        {props.isAdmin && (
          <div className="w-100">
            <button className="btn btn-primary">Сохранить</button>
            {order['id'] && (<button className="btn btn-warning" type="button" onClick={handleOrderRemove}>Отменить заказ</button>)}
            <button className="btn btn-secondary" type="button" onClick={props.onSave}>Закрыть</button>
          </div>
        )}
      </form>
    </>
  );
}
