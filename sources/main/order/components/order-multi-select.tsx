import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { OrderItem } from 'main/order/components/order-item';
import { Order } from 'main/order/models';
import { orderService } from 'main/order/services';

export interface OrderMultiSelectProps extends Omit<ResourceMultiSelectProps<Order>, 'service'> {
}

export function OrderMultiSelect(props: OrderMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      itemRender={OrderItem}
      service={orderService}
    />
  );
}
