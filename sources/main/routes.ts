import { lazy } from 'modules/lazy';
import { RouteProps } from 'react-router';

const AttachmentListPage = lazy(async () => (await import('main/attachment/containers/attachment-list-page')).AttachmentListPage);
const EmployeeListPage = lazy(async () => (await import('main/employee/containers/employee-list-page')).EmployeeListPage);
const ExampleListPage = lazy(async () => (await import('main/example/containers/example-list-page')).ExampleListPage);
const HolidayListPage = lazy(async () => (await import('main/holiday/containers/holiday-list-page')).HolidayListPage);
const OrderListPage = lazy(async () => (await import('main/order/containers/order-list-page')).OrderListPage);
const PositionListPage = lazy(async () => (await import('main/position/containers/position-list-page')).PositionListPage);
const RoleListPage = lazy(async () => (await import('main/role/containers/role-list-page')).RoleListPage);
const ServiceListPage = lazy(async () => (await import('main/service/containers/service-list-page')).ServiceListPage);
const ServiceTypeListPage = lazy(async () => (await import('main/service-type/containers/service-type-list-page')).ServiceTypeListPage);
const TimeSheetListPage = lazy(async () => (await import('main/time-sheet/containers/time-sheet-list-page')).TimeSheetListPage);
const UserListPage = lazy(async () => (await import('main/user/containers/user-list-page')).UserListPage);

export const routes: Array<RouteProps & { title: string }> = [
  { path: '/admin/attachments', title: 'Прикрепления', component: AttachmentListPage },
  { path: '/admin/employees', title: 'Сотрудники', component: EmployeeListPage },
  { path: '/admin/examples', title: 'Примеры работ', component: ExampleListPage },
  { path: '/admin/holidays', title: 'Отпуска', component: HolidayListPage },
  { path: '/admin/orders', title: 'Заказы', component: OrderListPage },
  { path: '/admin/positions', title: 'Должности', component: PositionListPage },
  { path: '/admin/roles', title: 'Роли', component: RoleListPage },
  { path: '/admin/services', title: 'Услуги', component: ServiceListPage },
  { path: '/admin/service-types', title: 'Типы услуг', component: ServiceTypeListPage },
  { path: '/admin/time-sheets', title: 'График работ', component: TimeSheetListPage },
  { path: '/admin/users', title: 'Пользователи', component: UserListPage },
];
