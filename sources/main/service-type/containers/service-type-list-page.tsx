import { useQueryString } from 'modules/hooks';
import { ResourceList } from 'modules/resource';
import React, { ReactElement } from 'react';
import { useHistory } from 'react-router';

import { ServiceTypeControls, ServiceTypeItem } from 'main/service-type/components';
import { serviceTypeService } from 'main/service-type/services';

export function ServiceTypeListPage(): ReactElement {
  const history = useHistory();
  const queryString = useQueryString();
  const searchParams = queryString.objectify(history.location.search);

  const handleResourceListItemUpsert = () => {
    history.push(history.location);
  };

  const handleResourceListSearchParamsChange = (event) => {
    history.push({ ...history.location, search: queryString.stringify(event.searchParams) });
  };

  return (
    <ResourceList
      controlsRender={ServiceTypeControls}
      itemRender={ServiceTypeItem}
      searchParams={searchParams}
      service={serviceTypeService}
      onItemUpsert={handleResourceListItemUpsert}
      onSearchParamsChange={handleResourceListSearchParamsChange}
    />
  );
}
