import { createResourceService } from 'modules/resource';

import { ServiceType } from 'main/service-type/models';

const service = createResourceService<ServiceType>({
  baseURL: '/api/service-types',
  dataItemKey: 'id',
  scope: 'services(serviceType())',
  textField: 'name',
});

export const serviceTypeService = Object.assign(service, {});
