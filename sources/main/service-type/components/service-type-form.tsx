import { Input } from '@progress/kendo-react-inputs';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { ServiceType } from 'main/service-type/models';
import { serviceTypeService } from 'main/service-type/services';

export interface ServiceTypeFormProps extends Omit<ResourceFormProps<ServiceType>, 'service'> {
}

export function ServiceTypeForm(props: ServiceTypeFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={serviceTypeService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Название">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.name ?? ''}
              onChange={(e) => setValue({ ...value, name: e.target.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
