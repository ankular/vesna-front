import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceTypeForm } from 'main/service-type/components/service-type-form';
import { ServiceType } from 'main/service-type/models';
import { serviceTypeService } from 'main/service-type/services';

export interface ServiceTypeControlsProps extends Omit<ResourceControlsProps<ServiceType>, 'service'> {
}

export function ServiceTypeControls(props: ServiceTypeControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={ServiceTypeForm}
      service={serviceTypeService}
    />
  );
}
