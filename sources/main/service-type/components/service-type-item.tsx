import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceType } from 'main/service-type/models';
import { serviceService } from 'main/service/services';

export interface ServiceTypeItemProps extends Omit<ResourceItemProps<ServiceType>, 'service'> {
}

export function ServiceTypeItem(props: ServiceTypeItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={serviceService}
    />
  );
}
