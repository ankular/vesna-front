import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ServiceTypeControls } from 'main/service-type/components/service-type-controls';
import { ServiceTypeItem } from 'main/service-type/components/service-type-item';
import { ServiceType } from 'main/service-type/models';
import { serviceTypeService } from 'main/service-type/services';

export interface ServiceTypeComboBoxProps extends Omit<ResourceComboBoxProps<ServiceType>, 'service'> {
}

export function ServiceTypeComboBox(props: ServiceTypeComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={ServiceTypeControls}
      itemRender={ServiceTypeItem}
      service={serviceTypeService}
    />
  );
}
