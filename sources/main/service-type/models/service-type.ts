import { Service } from 'main/service/models/service';

export interface ServiceType {
  id?: number;

  name?: string;

  services?: Service[];
}
