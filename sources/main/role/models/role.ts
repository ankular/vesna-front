import { LocalUser } from 'main/core/models';

export interface Role {
  id?: number;

  code?: string;
  name?: string;

  localUsers?: LocalUser[];
}
