import { createResourceService } from 'modules/resource';

import { Role } from 'main/role/models';

const service = createResourceService<Role>({
  baseURL: '/api/roles',
  dataItemKey: 'id',
  textField: 'name',
});

export const roleService = Object.assign(service, {});
