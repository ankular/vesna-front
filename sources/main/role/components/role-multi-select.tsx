import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { RoleControls } from 'main/role/components/role-controls';
import { RoleItem } from 'main/role/components/role-item';
import { Role } from 'main/role/models';
import { roleService } from 'main/role/services';

export interface RoleMultiSelectProps extends Omit<ResourceMultiSelectProps<Role>, 'service'> {
}

export function RoleMultiSelect(props: RoleMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={RoleControls}
      itemRender={RoleItem}
      service={roleService}
    />
  );
}
