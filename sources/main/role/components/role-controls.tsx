import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { RoleForm } from 'main/role/components/role-form';
import { Role } from 'main/role/models';
import { roleService } from 'main/role/services';

export interface RoleControlsProps extends Omit<ResourceControlsProps<Role>, 'service'> {
}

export function RoleControls(props: RoleControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={RoleForm}
      service={roleService}
    />
  );
}
