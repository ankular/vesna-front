import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { Role } from 'main/role/models';
import { roleService } from 'main/role/services';

export interface RoleItemProps extends Omit<ResourceItemProps<Role>, 'service'> {
}

export function RoleItem(props: RoleItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={roleService}
    />
  );
}
