import { Input } from '@progress/kendo-react-inputs';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Role } from 'main/role/models';
import { roleService } from 'main/role/services';

export interface RoleFormProps extends Omit<ResourceFormProps<Role>, 'service'> {
}

export function RoleForm(props: RoleFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={roleService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Название">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.name ?? ''}
              onChange={(e) => setValue({ ...value, name: e.target.value })}
            />
          </FormGroup>

          <FormGroup label="Код">
            <Input
              className="w-100"
              disabled={submitted}
              pattern="[a-zA-Z0-9]+"
              placeholder="Код"
              value={value.code ?? ''}
              onChange={(e) => setValue({ ...value, code: e.target.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
