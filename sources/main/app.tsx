import { EffectResolver, ErrorBoundary, Loader } from 'modules/components';
import React, { ReactElement } from 'react';

import { useStoreGetter, useStoreSetter } from 'crunch-store';
import { AdminPage, AuthPage, MainPage } from 'main/core/containers';
import { authService } from 'main/core/services';
import { RootStore } from 'main/store';
import { useLocation } from 'react-router';

export function App(): ReactElement {
  const { getIsAuthenticated } = useStoreGetter<RootStore>().authStore;
  const { setCurrentUser } = useStoreSetter<RootStore>().authStore;
  const authCheck = () => authService.check().then(setCurrentUser);
  const isAuthenticated = getIsAuthenticated();
  const location = useLocation();
  const isAdminPage = location.pathname.startsWith('/admin');

  return (
    <ErrorBoundary>
      {isAdminPage && (
        <EffectResolver
          effect={authCheck}
          fallback={Loader}
          children={() => isAuthenticated ? (<AdminPage/>) : (<AuthPage/>)}
        />
      )}

      {!isAdminPage && (<MainPage/>)}
    </ErrorBoundary>
  );
}
