import { Attachment } from 'main/attachment/models';
import { LocalUser } from 'main/core/models';

export interface User {
  id?: number;

  bornAt?: string;
  phone?: string;
  fullName?: string;

  avatarId?: number;

  readonly age?: number;
  readonly avatarUrl?: string;

  avatar?: Attachment;
  localUser?: LocalUser;
}
