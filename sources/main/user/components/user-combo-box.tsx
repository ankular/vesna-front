import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { UserControls } from 'main/user/components/user-controls';
import { UserItem } from 'main/user/components/user-item';
import { User } from 'main/user/models';
import { userService } from 'main/user/services';

export interface UserComboBoxProps extends Omit<ResourceComboBoxProps<User>, 'service'> {
}

export function UserComboBox(props: UserComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={UserControls}
      itemRender={UserItem}
      service={userService}
    />
  );
}
