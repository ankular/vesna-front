import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { UserControls } from 'main/user/components/user-controls';
import { UserItem } from 'main/user/components/user-item';
import { User } from 'main/user/models';
import { userService } from 'main/user/services';

export interface UserMultiSelectProps extends Omit<ResourceMultiSelectProps<User>, 'service'> {
}

export function UserMultiSelect(props: UserMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={UserControls}
      itemRender={UserItem}
      service={userService}
    />
  );
}
