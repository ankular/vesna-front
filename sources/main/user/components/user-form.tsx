import { DatePicker } from '@progress/kendo-react-dateinputs';
import { Input, MaskedTextBox } from '@progress/kendo-react-inputs';
import { DateTime } from 'luxon';
import { RoleMultiSelect } from 'main/role/components';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { AttachmentComboBox } from 'main/attachment/components';
import { User } from 'main/user/models';
import { userService } from 'main/user/services';

export interface UserFormProps extends Omit<ResourceFormProps<User>, 'service'> {
}

export function UserForm(props: UserFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={userService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="ФИО">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.fullName ?? ''}
              onChange={(e) => setValue({ ...value, fullName: e.target.value })}
            />
          </FormGroup>

          <FormGroup label="Дата рождения">
            <DatePicker
              className="w-100"
              disabled={submitted}
              format="dd.MM.yyyy"
              max={DateTime.local().toJSDate()}
              value={value.bornAt ? DateTime.fromISO(value.bornAt).toJSDate() : undefined}
              onChange={(e) => setValue({ ...value, bornAt: DateTime.fromJSDate(e.target.value).toISO() })}
            />
          </FormGroup>

          <FormGroup label="Телефон">
            <MaskedTextBox
              disabled={submitted}
              mask="+7 (000) 000-00-00"
              placeholder="+7 (000) 000-00-00"
              value={value.phone ?? ''}
              onChange={(e) => setValue({ ...value, phone: e.target.rawValue.trim() ? e.target.value : undefined })}
            />
          </FormGroup>

          <FormGroup label="Аватар">
            <AttachmentComboBox
              disabled={submitted}
              value={value.avatar}
              onChange={(e) => setValue({ ...value, avatar: e.value, avatarId: e.value.id })}
            />
          </FormGroup>

          {value.localUser != null && (
            <FormGroup label="Роли">
              <RoleMultiSelect
                disabled={submitted}
                value={value.localUser.roles ?? []}
                onChange={(e) => setValue({ ...value, localUser: { ...value.localUser, roles: e.value } })}
              />
            </FormGroup>
          )}
        </Fragment>
      )}
    />
  );
}
