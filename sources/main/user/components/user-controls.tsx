import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { UserForm } from 'main/user/components/user-form';
import { User } from 'main/user/models';
import { userService } from 'main/user/services';

export interface UserControlsProps extends Omit<ResourceControlsProps<User>, 'service'> {
}

export function UserControls(props: UserControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={UserForm}
      service={userService}
    />
  );
}
