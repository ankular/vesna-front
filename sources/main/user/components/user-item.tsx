import { Avatar } from '@progress/kendo-react-layout';
import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { User } from 'main/user/models';
import { userService } from 'main/user/services';

export interface UserItemProps extends Omit<ResourceItemProps<User>, 'service'> {
}

export function UserItem(props: UserItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={userService}
      children={({ value }) => value && (
        <Fragment>
          <Avatar shape="circle" type={value.id ? 'image' : 'icon'}>
            {value.id && (<img alt={`${value.id}`} src={`${value.avatarUrl}?resize=32`}/>)}
            {!value.id && (<i className="fas fa-user-alt mx-1"/>)}
          </Avatar>
          {value.fullName}
        </Fragment>
      )}
    />
  );
}
