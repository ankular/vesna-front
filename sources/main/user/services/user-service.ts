import { createResourceService } from 'modules/resource';

import { User } from 'main/user/models';

const service = createResourceService<User>({
  baseURL: '/api/users',
  dataItemKey: 'id',
  scope: 'avatar(),localUser(roles())',
  textField: 'fullName',
});

export const userService = Object.assign(service, {});
