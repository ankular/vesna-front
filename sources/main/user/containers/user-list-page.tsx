import { useQueryString } from 'modules/hooks';
import { ResourceList } from 'modules/resource';
import React, { ReactElement } from 'react';
import { useHistory } from 'react-router';

import { UserControls, UserItem } from 'main/user/components';
import { userService } from 'main/user/services';

export function UserListPage(): ReactElement {
  const history = useHistory();
  const queryString = useQueryString();
  const searchParams = queryString.objectify(history.location.search);

  const handleResourceListItemUpsert = () => {
    history.push(history.location);
  };

  const handleResourceListSearchParamsChange = (event) => {
    history.push({ ...history.location, search: queryString.stringify(event.searchParams) });
  };

  return (
    <ResourceList
      controlsRender={UserControls}
      itemRender={UserItem}
      searchParams={searchParams}
      service={userService}
      onItemUpsert={handleResourceListItemUpsert}
      onSearchParamsChange={handleResourceListSearchParamsChange}
    />
  );
}
