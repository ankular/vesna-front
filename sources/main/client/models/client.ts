import { Order } from 'main/order/models';
import { User } from 'main/user/models';

export interface Client {
  id?: number;
  userId?: number;

  orders?: Order[];
  user: User;
}
