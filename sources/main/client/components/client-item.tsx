import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';

export interface ClientItemProps extends Omit<ResourceItemProps<Client>, 'service'> {
}

export function ClientItem(props: ClientItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      extract
      service={clientService}
      children={({ value }) => value && (
        <div className="d-flex flex-column w-100">
          <div className="d-flex justify-content-between">
            <h5 className="mb-1">{value.user.fullName}</h5>
            <small>{value['id']}</small>
          </div>
        </div>
      )}
    />
  );
}
