import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ClientControls } from 'main/client/components/client-controls';
import { ClientItem } from 'main/client/components/client-item';
import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';

export interface ClientMultiSelectProps extends Omit<ResourceMultiSelectProps<Client>, 'service'> {
}

export function ClientMultiSelect(props: ClientMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={ClientControls}
      itemRender={ClientItem}
      service={clientService}
    />
  );
}
