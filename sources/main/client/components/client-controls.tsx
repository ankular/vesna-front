import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ClientForm } from 'main/client/components/client-form';
import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';

export interface ClientControlsProps extends Omit<ResourceControlsProps<Client>, 'service'> {
}

export function ClientControls(props: ClientControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={ClientForm}
      service={clientService}
    />
  );
}
