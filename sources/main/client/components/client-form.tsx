import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';
import { UserComboBox } from 'main/user/components';
import { PositionMultiSelect } from 'main/position/components';
import { ServiceMultiSelect } from 'main/service/components';

export interface ClientFormProps extends Omit<ResourceFormProps<Client>, 'service'> {
}

export function ClientForm(props: ClientFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={clientService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Пользователь">
            <UserComboBox
              disabled={submitted}
              required
              value={value.user}
              onChange={(e) => setValue({ ...value, user: e.value, userId: e.value.id })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
