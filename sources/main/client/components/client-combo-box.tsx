import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ClientControls } from 'main/client/components/client-controls';
import { ClientItem } from 'main/client/components/client-item';
import { Client } from 'main/client/models';
import { clientService } from 'main/client/services';

export interface ClientComboBoxProps extends Omit<ResourceComboBoxProps<Client>, 'service'> {
}

export function ClientComboBox(props: ClientComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={ClientControls}
      itemRender={ClientItem}
      service={clientService}
    />
  );
}
