import { createResourceService } from 'modules/resource';

import { Client } from 'main/client/models';

const service = createResourceService<Client>({
  baseURL: '/api/clients',
  dataItemKey: 'id',
  scope: 'user()',
  textField: 'user.fullName',
});

export const clientService = Object.assign(service, {});
