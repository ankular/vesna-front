import { Holiday } from 'main/holiday/models';
import { Position } from 'main/position/models';
import { Service } from 'main/service/models';
import { TimeSheet } from 'main/time-sheet/models';
import { User } from 'main/user/models';

export interface Employee {
  id?: number;

  userId?: number;

  holidays?: Holiday[];
  positions?: Position[];
  services?: Service[];
  timeSheets?: TimeSheet[];
  user?: User;
}
