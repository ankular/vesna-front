import { Employee } from 'main/employee/models/employee';
import { Order } from 'main/order/models';
import { Service } from 'main/service/models';

export interface EmployeeService {
  id?: number;

  leadTime?: string;

  employeeId?: number;
  serviceId?: number;

  employee?: Employee;
  orders?: Order[];
  orderEmployeeServices?;
  service?: Service;
}
