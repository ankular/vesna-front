import { createResourceService } from 'modules/resource';

import { Employee } from 'main/employee/models';

const service = createResourceService<Employee>({
  baseURL: '/api/employees',
  dataItemKey: 'id',
  scope: `
    positions(),
    services(
      serviceType(),
      employeeServices(),
    ),
    user(avatar()),
  `.replace(/\n\r|\n|\r|\s/g, ''),
  textField: 'user.fullName',
});

const employeeServiceQuery = service.query as any;

export const employeeService = Object.assign(service, {
  query: (...args) => {
    return employeeServiceQuery(...args)
      .then((result) => {
        result.data = result.data.map((dataItem) => {
          dataItem.services = dataItem.services.map((dataItemService) => {
            const [hour, minute] = dataItemService['employeeServices'][0]?.leadTime.split(':');
            dataItemService['leadTime'] = { hour: +hour, minute: +minute };
            return dataItemService;
          });
          return dataItem;
        });
        return result;
      });
  },
});
