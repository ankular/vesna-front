import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { EmployeeControls } from 'main/employee/components/employee-controls';
import { EmployeeItem } from 'main/employee/components/employee-item';
import { Employee } from 'main/employee/models';
import { employeeService } from 'main/employee/services';

export interface EmployeeComboBoxProps extends Omit<ResourceComboBoxProps<Employee>, 'service'> {
}

export function EmployeeComboBox(props: EmployeeComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={EmployeeControls}
      itemRender={EmployeeItem}
      service={employeeService}
    />
  );
}
