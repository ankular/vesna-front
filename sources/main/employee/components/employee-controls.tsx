import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { EmployeeForm } from 'main/employee/components/employee-form';
import { Employee } from 'main/employee/models';
import { employeeService } from 'main/employee/services';

export interface EmployeeControlsProps extends Omit<ResourceControlsProps<Employee>, 'service'> {
}

export function EmployeeControls(props: EmployeeControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={EmployeeForm}
      service={employeeService}
    />
  );
}
