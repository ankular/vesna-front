import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { EmployeeControls } from 'main/employee/components/employee-controls';
import { EmployeeItem } from 'main/employee/components/employee-item';
import { Employee } from 'main/employee/models';
import { employeeService } from 'main/employee/services';

export interface EmployeeMultiSelectProps extends Omit<ResourceMultiSelectProps<Employee>, 'service'> {
}

export function EmployeeMultiSelect(props: EmployeeMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={EmployeeControls}
      itemRender={EmployeeItem}
      service={employeeService}
    />
  );
}
