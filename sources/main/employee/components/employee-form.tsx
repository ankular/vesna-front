import { NumericTextBox } from '@progress/kendo-react-inputs';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Employee } from 'main/employee/models';
import { employeeService } from 'main/employee/services';
import { UserComboBox } from 'main/user/components';
import { PositionMultiSelect } from 'main/position/components';
import { ServiceMultiSelect } from 'main/service/components';

export interface EmployeeFormProps extends Omit<ResourceFormProps<Employee>, 'service'> {
}

export function EmployeeForm(props: EmployeeFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={employeeService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Пользователь">
            <UserComboBox
              disabled={submitted}
              required
              value={value.user}
              onChange={(e) => setValue({ ...value, userId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Должности">
            <PositionMultiSelect
              disabled={submitted}
              value={value.positions}
              onChange={(e) => setValue({ ...value, positions: e.value })}
            />
          </FormGroup>

          <FormGroup label="Услуги">
            <ServiceMultiSelect
              disabled={submitted}
              value={value.services}
              onChange={(e) => setValue({ ...value, services: e.value })}
            />
          </FormGroup>

          {value.services.length > 0 && (<table className="table table-sm">
            <thead>
            <tr>
              <th>Название</th>
              <th>Время выполнения</th>
            </tr>
            </thead>
            <tbody>
            {value.services?.map((service, index) => (
              <tr key={index}>
                <td>{service.name}</td>

                <td>
                  <NumericTextBox
                    width={60}
                    min={0}
                    max={4}
                    spinners={false}
                    placeholder="час."
                    value={value.services[index]?.['leadTime']?.hour}
                    onChange={(e) => {
                      value.services[index]['leadTime'] = value.services[index]['leadTime'] ?? {};
                      value.services[index]['leadTime'].hour = e.value;
                      setValue({ ...value });
                    }}
                  />ч.

                  <NumericTextBox
                    width={60}
                    min={0}
                    max={60}
                    spinners={false}
                    placeholder="мин."
                    value={value.services[index]?.['leadTime']?.minute}
                    onChange={(e) => {
                      value.services[index]['leadTime'] = value.services[index]['leadTime'] ?? {};
                      value.services[index]['leadTime'].minute = e.value;
                      setValue({ ...value });
                    }}
                  />м.
                </td>
              </tr>
            ))}
            </tbody>
          </table>)}
        </Fragment>
      )}
    />
  );
}
