import { useQueryString } from 'modules/hooks';
import { ResourceList } from 'modules/resource';
import React, { ReactElement } from 'react';
import { useHistory } from 'react-router';

import { EmployeeControls, EmployeeItem } from 'main/employee/components';
import { employeeService } from 'main/employee/services';

export function EmployeeListPage(): ReactElement {
  const history = useHistory();
  const queryString = useQueryString();
  const searchParams = queryString.objectify(history.location.search);

  const handleResourceListItemUpsert = () => {
    history.push(history.location);
  };

  const handleResourceListSearchParamsChange = (event) => {
    history.push({ ...history.location, search: queryString.stringify(event.searchParams) });
  };

  return (
    <ResourceList
      controlsRender={EmployeeControls}
      itemRender={EmployeeItem}
      searchParams={searchParams}
      service={employeeService}
      onItemUpsert={handleResourceListItemUpsert}
      onSearchParamsChange={handleResourceListSearchParamsChange}
    />
  );
}
