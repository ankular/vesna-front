import { createResourceService } from 'modules/resource';

import { Attachment } from 'main/attachment/models';

const service = createResourceService<Attachment>({
  baseURL: '/api/attachments',
  dataItemKey: 'id',
  scope: 'examples(service(serviceType()),employee(user()))',
  textField: 'name',
});

export const attachmentService = Object.assign(service, {});
