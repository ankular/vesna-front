import { Example } from 'main/example/models/example';
import { Service } from 'main/service/models/service';
import { User } from 'main/user/models';

export interface Attachment {
  id?: number;

  file?: string;

  directory?: string;
  mime?: string;
  name?: string;
  size?: number;

  readonly isImage?: boolean;
  readonly url?: string;

  examples?: Example[];
  serviceImages?: Service[];
  userAvatars?: User[];
}
