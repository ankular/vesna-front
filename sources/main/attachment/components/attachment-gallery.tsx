import { Dialog } from '@progress/kendo-react-dialogs';
import { usePortal } from 'modules/portal';
import React, { ReactElement, useEffect, useState } from 'react';

import { AttachmentCarousel } from 'main/attachment/components/attachment-carousel';
import { Attachment } from 'main/attachment/models';

export interface AttachmentGalleryProps {
  data: Attachment[];
  children?: (value) => ReactElement;
}

export function AttachmentGallery(props: AttachmentGalleryProps): ReactElement {
  const [selected, setSelected] = useState<number>();

  const attachmentCarouselDialog = usePortal((
    <Dialog title="Просмотр" onClose={() => setSelected(undefined)}>
      <AttachmentCarousel data={props.data} children={props.children} selected={selected} onSelect={(e) => setSelected(e.selected)}/>
    </Dialog>
  ), 'AttachmentCarouselDialog');

  useEffect(() => attachmentCarouselDialog.toggle(selected != null), [selected != null]);

  return (
    <div className="card-columns">
      {props.data?.map((dataItem, index) => (
        <div key={index} className="card" onClick={() => {
          setSelected(index);
        }}>
          <img alt={dataItem.name} className="card-img" src={dataItem.url}/>
        </div>
      ))}
    </div>
  );
}
