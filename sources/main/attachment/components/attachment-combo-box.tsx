import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { AttachmentControls } from 'main/attachment/components/attachment-controls';
import { AttachmentItem } from 'main/attachment/components/attachment-item';
import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';

export interface AttachmentComboBoxProps extends Omit<ResourceComboBoxProps<Attachment>, 'service'> {
}

export function AttachmentComboBox(props: AttachmentComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={AttachmentControls}
      itemRender={AttachmentItem}
      service={attachmentService}
    />
  );
}
