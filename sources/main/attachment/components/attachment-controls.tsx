import { Dialog } from '@progress/kendo-react-dialogs';
import { usePortal } from 'modules/portal';
import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { AttachmentForm } from 'main/attachment/components/attachment-form';
import { AttachmentCarousel } from 'main/attachment/components/attachment-carousel';
import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';

export interface AttachmentControlsProps extends Omit<ResourceControlsProps<Attachment>, 'service'> {
}

export function AttachmentControls(props: AttachmentControlsProps): ReactElement {
  const attachmentCarouselDialog = usePortal((
    <Dialog title="Просмотр" onClose={() => attachmentCarouselDialog.hide()}>
      <AttachmentCarousel data={[props.value]}/>
    </Dialog>
  ), 'AttachmentCarouselDialog');

  return (
    <ResourceControls
      {...props}
      formRender={AttachmentForm}
      service={attachmentService}
      children={({ value }) => value && (
        <Fragment>
          {value.id != null && value.isImage && (
            <button className="btn btn-secondary" type="button" onClick={() => attachmentCarouselDialog.show()}>
              <i className="fa fa-search-plus"/>
            </button>
          )}

          {value.id != null && (
            <a className="btn btn-secondary" href={value.url} target="_blank">
              <i className="fa fa-download"/>
            </a>
          )}
        </Fragment>
      )}
    />
  );
}
