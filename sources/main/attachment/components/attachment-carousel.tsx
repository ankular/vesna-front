import React, { ReactElement, useState, Fragment } from 'react';

import { Attachment } from 'main/attachment/models';

export interface AttachmentCarouselProps {
  data: Attachment[];
  selected?: number;
  onSelect?: (event: { selected: number }) => void;
  children?: (value) => ReactElement;
}

export function AttachmentCarousel(props: AttachmentCarouselProps): ReactElement {
  const [selected, setSelected] = useState<number>(props.selected ?? 0);

  const handleSelect = (value: number) => {
    setSelected(value);
    props.onSelect?.({ selected: value });
  };

  return (
    <div className="carousel">
      <ol className="carousel-indicators">
        {props.data.map((dataItem, index) => (
          <li key={index} className={index === selected ? 'active' : ''} onClick={() => handleSelect(index)}/>
        ))}
      </ol>

      <div className="carousel-inner">
        {props.data.map((dataItem, index) => (
          <Fragment key={index}>
            <div className={`carousel-item${index === selected ? ' active' : ''}`}>
              <img alt={dataItem.name} className="d-block w-100" src={dataItem.url} />
              <div className="carousel-caption d-none d-md-block"/>
            </div>
          </Fragment>
        ))}
      </div>

      {selected > 0 && (
        <a className="border-0 bg-transparent carousel-control-prev" onClick={() => handleSelect(selected - 1)}>
          <i className="fas fa-chevron-left" aria-hidden="true"/>
          <span className="sr-only">Предыдущая</span>
        </a>
      )}

      {selected < props.data.length - 1 && (
        <button className="border-0 bg-transparent carousel-control-next" onClick={() => handleSelect(selected + 1)}>
          <span className="fas fa-chevron-right text-dark" aria-hidden="true"/>
          <i className="sr-only">Следующая</i>
        </button>
      )}

      {selected != null && (<div>
        {!props.children && props.data[selected].name}
        {props.children && props.children(props.data[selected])}
      </div>)}
    </div>
  );
}
