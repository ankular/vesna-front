import { Input } from '@progress/kendo-react-inputs';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormChildrenProps, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement, SyntheticEvent } from 'react';

import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';

export interface AttachmentFormProps extends Omit<ResourceFormProps<Attachment>, 'service'> {
}

export function AttachmentForm(props: AttachmentFormProps): ReactElement {
  const handleInputFileChange = (event: SyntheticEvent, childrenProps: ResourceFormChildrenProps<Attachment>) => {
    const files = event.target['element'].files ?? [];

    if (files.length > 0) {
      const fileData: File = files[0];
      const fileReader = new FileReader();

      childrenProps.setSubmitted(true);
      fileReader.readAsDataURL(fileData);

      fileReader.onload = () => {
        childrenProps.setValue({
          ...childrenProps.value,
          file: `${fileReader.result}`.split(',').pop(),
          mime: fileData.type,
          name: fileData.name,
          size: fileData.size,
        });

        childrenProps.setSubmitted(false);
      };
    }
  };

  return (
    <ResourceForm
      {...props}
      service={attachmentService}
      children={({ submitted, value, setSubmitted, setValue }) => (
        <Fragment>
          <FormGroup label="Название">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.name ?? ''}
              onChange={(e) => setValue({ ...value, name: e.target.value })}
            />
          </FormGroup>

          <FormGroup label="Файл">
            <Input
              accept="image/*"
              className="w-100"
              disabled={submitted}
              required={!value.id}
              type="file"
              onChange={(e) => handleInputFileChange(e, { submitted, value, setSubmitted, setValue })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
