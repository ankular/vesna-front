import { Avatar } from '@progress/kendo-react-layout';
import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';

export interface AttachmentItemProps extends Omit<ResourceItemProps<Attachment>, 'service'> {
}

export function AttachmentItem(props: AttachmentItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={attachmentService}
      children={({ value }) => value && (
        <>
          <Avatar shape="circle" type={value.isImage && value.url ? 'image' : 'icon'}>
            {value.isImage && value.url && (<img alt={`${value.id}`} src={`${value.url}?resize=32`}/>)}
            {!value.isImage && (<i className={`fas fa-${value.id ? 'file-alt' : 'question'} mx-1`}/>)}
          </Avatar>
          {value.name}
        </>
      )}
    />
  );
}
