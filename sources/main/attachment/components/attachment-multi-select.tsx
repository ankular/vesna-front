import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { AttachmentControls } from 'main/attachment/components/attachment-controls';
import { AttachmentItem } from 'main/attachment/components/attachment-item';
import { Attachment } from 'main/attachment/models';
import { attachmentService } from 'main/attachment/services';

export interface AttachmentMultiSelectProps extends Omit<ResourceMultiSelectProps<Attachment>, 'service'> {
}

export function AttachmentMultiSelect(props: AttachmentMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={AttachmentControls}
      itemRender={AttachmentItem}
      service={attachmentService}
    />
  );
}
