import { createStore } from 'crunch-store';

import { authStore } from 'main/core/stores';

export const rootStore = createStore({ authStore });
export type RootStore = ReturnType<typeof rootStore>;
