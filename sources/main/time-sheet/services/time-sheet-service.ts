import { createResourceService } from 'modules/resource';

import { TimeSheet } from 'main/time-sheet/models';

const service = createResourceService<TimeSheet>({
  baseURL: '/api/time-sheets',
  dataItemKey: 'id',
  scope: 'employee(user())',
  textField: 'employee.user.fullName',
});

export const timeSheetService = Object.assign(service, {});
