import { ResourceComboBox, ResourceComboBoxProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { TimeSheetControls } from 'main/time-sheet/components/time-sheet-controls';
import { TimeSheetItem } from 'main/time-sheet/components/time-sheet-item';
import { TimeSheet } from 'main/time-sheet/models';
import { timeSheetService } from 'main/time-sheet/services';

export interface TimeSheetComboBoxProps extends Omit<ResourceComboBoxProps<TimeSheet>, 'service'> {
}

export function TimeSheetComboBox(props: TimeSheetComboBoxProps): ReactElement {
  return (
    <ResourceComboBox
      {...props}
      controlsRender={TimeSheetControls}
      itemRender={TimeSheetItem}
      service={timeSheetService}
    />
  );
}
