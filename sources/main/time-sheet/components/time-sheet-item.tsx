import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement, Fragment } from 'react';

import { TimeSheet } from 'main/time-sheet/models';
import { timeSheetService } from 'main/time-sheet/services';

export interface TimeSheetItemProps extends Omit<ResourceItemProps<TimeSheet>, 'service'> {
}

export function TimeSheetItem(props: TimeSheetItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      extract
      service={timeSheetService}
      children={({ value }) => value && (
        <div className="d-flex flex-column w-100">
          <div className="d-flex justify-content-between">
            <h5 className="mb-1">{value.employee.user.fullName}</h5>
          </div>

          <div className="d-flex">
            <span>{value.startWorkDate} {value.endWorkDate != null && <>  - {value.endWorkDate}</>}&nbsp;</span>
            <small>({value.startWorkTime} - {value.endWorkTime})</small>
          </div>

          <div>{value.workdayCount}/{value.weekendCount}</div>
          {/*

          {value.positions?.length > 0 && (
            <div className="mb-1">
              <b>Должности: </b>
              {value.positions.map((position, index, positions) => (
                <span key={index}>{position.name}{positions.length - 1 > index && (', ')}</span>
              ))}
            </div>
          )}

          {value.services?.length > 0 && (
            <div className="mb-1">
              <b>Услуги: </b>
              {value.services.map((service, index, services) => (
                <Fragment key={index}>
                  {service.name}
                  <small> ({service.serviceType.name})</small>
                  {services.length - 1 > index && (', ')}
                </Fragment>
              ))}
            </div>
          )}*/}
        </div>
      )}
    />
  );
}
