import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { TimeSheet } from 'main/time-sheet/models';
import { timeSheetService } from 'main/time-sheet/services';
import { DatePicker, TimePicker } from '@progress/kendo-react-dateinputs';
import { DateTime } from 'luxon';
import { NumericTextBox } from '@progress/kendo-react-inputs';
import { EmployeeComboBox } from 'main/employee/components';

export interface TimeSheetFormProps extends Omit<ResourceFormProps<TimeSheet>, 'service'> {
}

export function TimeSheetForm(props: TimeSheetFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={timeSheetService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Сотрудник">
            <EmployeeComboBox
              controls={false}
              disabled={submitted}
              required
              value={value.employee}
              onChange={(e) => setValue({ ...value, /*employee: e.value, */employeeId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Период работы">
            <div className="align-items-center d-flex justify-content-between">
              <DatePicker
                className="flex-grow-1"
                disabled={submitted}
                format="dd.MM.yyyy"
                max={value.endWorkDate ? DateTime.fromISO(value.endWorkDate).toJSDate() : DateTime.local().toJSDate()}
                required
                value={value.startWorkDate ? DateTime.fromISO(value.startWorkDate).toJSDate() : undefined}
                onChange={(e) => setValue({ ...value, startWorkDate: DateTime.fromJSDate(e.target.value).toFormat('yyyy-MM-dd') })}
              />

              <span className="mx-2">-</span>

              <DatePicker
                className="flex-grow-1"
                disabled={submitted}
                format="dd.MM.yyyy"
                min={value.startWorkDate ? DateTime.fromISO(value.startWorkDate).toJSDate() : undefined}
                value={value.endWorkDate ? DateTime.fromISO(value.endWorkDate).toJSDate() : undefined}
                onChange={(e) => setValue({ ...value, endWorkDate: DateTime.fromJSDate(e.target.value).toFormat('yyyy-MM-dd') })}
              />
            </div>
          </FormGroup>

          <FormGroup label="Рабочая смена">
            <div className="align-items-center d-flex justify-content-between">
              <TimePicker
                className="flex-grow-1"
                disabled={submitted}
                format="HH:mm"
                max={value.endWorkTime ? DateTime.fromISO(value.endWorkTime).toJSDate() : DateTime.local().toJSDate()}
                required
                value={value.startWorkTime ? DateTime.fromISO(value.startWorkTime).toJSDate() : undefined}
                onChange={(e) => setValue({ ...value, startWorkTime: DateTime.fromJSDate(e.target.value).toFormat('HH:mm:ss') })}
              />

              <span className="mx-2">-</span>

              <TimePicker
                className="flex-grow-1"
                disabled={submitted}
                format="HH:mm"
                min={value.startWorkTime ? DateTime.fromISO(value.startWorkTime).toJSDate() : undefined}
                required
                value={value.endWorkTime ? DateTime.fromISO(value.endWorkTime).toJSDate() : undefined}
                onChange={(e) => setValue({ ...value, endWorkTime: DateTime.fromJSDate(e.target.value).toFormat('HH:mm:ss') })}
              />
            </div>
          </FormGroup>

          <FormGroup label="Рабочие дни">
            <div className="align-items-center d-flex justify-content-between">
              <NumericTextBox
                className="flex-grow-1"
                disabled={submitted}
                min={1}
                required
                value={value.workdayCount}
                onChange={(e) => setValue({ ...value, workdayCount: e.target.value })}
              />

              <span className="mx-2">/</span>

              <NumericTextBox
                className="flex-grow-1"
                disabled={submitted}
                min={1}
                required
                value={value.weekendCount}
                onChange={(e) => setValue({ ...value, weekendCount: e.target.value })}
              />
            </div>
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
