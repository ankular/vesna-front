import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { TimeSheetControls } from 'main/time-sheet/components/time-sheet-controls';
import { TimeSheetItem } from 'main/time-sheet/components/time-sheet-item';
import { TimeSheet } from 'main/time-sheet/models';
import { timeSheetService } from 'main/time-sheet/services';

export interface TimeSheetMultiSelectProps extends Omit<ResourceMultiSelectProps<TimeSheet>, 'service'> {
}

export function TimeSheetMultiSelect(props: TimeSheetMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={TimeSheetControls}
      itemRender={TimeSheetItem}
      service={timeSheetService}
    />
  );
}
