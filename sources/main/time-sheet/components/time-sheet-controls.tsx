import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { TimeSheetForm } from 'main/time-sheet/components/time-sheet-form';
import { TimeSheet } from 'main/time-sheet/models';
import { timeSheetService } from 'main/time-sheet/services';

export interface TimeSheetControlsProps extends Omit<ResourceControlsProps<TimeSheet>, 'service'> {
}

export function TimeSheetControls(props: TimeSheetControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={TimeSheetForm}
      service={timeSheetService}
    />
  );
}
