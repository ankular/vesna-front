import { Employee } from 'main/employee/models';

export interface TimeSheet {
  endWorkDate?: string;
  endWorkTime?: string;
  startWorkDate?: string;
  startWorkTime?: string;
  weekendCount?: number;
  workdayCount?: number;

  employeeId?: number;

  employee?: Employee;
}
