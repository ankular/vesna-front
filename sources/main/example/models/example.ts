import { Attachment } from 'main/attachment/models';
import { Employee } from 'main/employee/models';
import { Service } from 'main/service/models';

export interface Example {
  description?: string;

  employeeId?: number;
  serviceId?: number;

  attachments?: Attachment[];
  employee?: Employee;
  service?: Service;
}
