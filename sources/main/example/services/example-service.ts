import { createResourceService } from 'modules/resource';

import { Example } from 'main/example/models';

const example = createResourceService<Example>({
  baseURL: '/api/examples',
  dataItemKey: 'id',
  scope: 'attachments(),employee(user()),service(serviceType())',
  textField: 'name',
});

export const exampleService = Object.assign(example, {});
