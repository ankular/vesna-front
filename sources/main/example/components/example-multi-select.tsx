import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ExampleControls } from 'main/example/components/example-controls';
import { ExampleItem } from 'main/example/components/example-item';
import { Example } from 'main/example/models';
import { exampleService } from 'main/example/services';

export interface ExampleMultiSelectProps extends Omit<ResourceMultiSelectProps<Example>, 'service'> {
}

export function ExampleMultiSelect(props: ExampleMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={ExampleControls}
      itemRender={ExampleItem}
      service={exampleService}
    />
  );
}
