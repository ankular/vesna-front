import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { ExampleForm } from 'main/example/components/example-form';
import { Example } from 'main/example/models';
import { exampleService } from 'main/example/services';

export interface ExampleControlsProps extends Omit<ResourceControlsProps<Example>, 'example'> {
}

export function ExampleControls(props: ExampleControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={ExampleForm}
      service={exampleService}
    />
  );
}
