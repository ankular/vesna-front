import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement, Fragment } from 'react';

import { Example } from 'main/example/models';
import { exampleService } from 'main/example/services';
import { ServiceItem } from 'main/service/components';

export interface ExampleItemProps extends Omit<ResourceItemProps<Example>, 'service'> {
}

export function ExampleItem(props: ExampleItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={exampleService}
      children={({ value }) => value && (
        <div>
          <span>{value.employee.user.fullName}</span>
          <ServiceItem value={value.service}/>
        </div>
      )}
    />
  );
}
