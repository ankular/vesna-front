import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Example } from 'main/example/models';
import { exampleService } from 'main/example/services';
import { EmployeeComboBox } from 'main/employee/components';
import { ServiceComboBox } from 'main/service/components/service-combo-box';
import { AttachmentMultiSelect } from 'main/attachment/components';

export interface ExampleFormProps extends Omit<ResourceFormProps<Example>, 'example'> {
}

export function ExampleForm(props: ExampleFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={exampleService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Сотрудник">
            <EmployeeComboBox
              disabled={submitted}
              required
              value={value.employee}
              onChange={(e) => setValue({ ...value, /*employee: e.value, */employeeId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Услуга">
            <ServiceComboBox
              disabled={submitted}
              required
              value={value.service}
              onChange={(e) => setValue({ ...value, /*service: e.value, */serviceId: e.value.id })}
            />
          </FormGroup>

          <FormGroup label="Прикрепления">
            <AttachmentMultiSelect
              disabled={submitted}
              value={value.attachments}
              onChange={(e) => setValue({ ...value, attachments: e.value })}
            />
          </FormGroup>

          <FormGroup label="Описание">
            <textarea
              className="k-textarea w-100"
              disabled={submitted}
              value={value.description ?? ''}
              onChange={(e) => setValue({ ...value, description: e.target.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
