import { useQueryString } from 'modules/hooks';
import { ResourceList } from 'modules/resource';
import React, { ReactElement } from 'react';
import { useHistory } from 'react-router';

import { ExampleControls, ExampleItem } from 'main/example/components';
import { exampleService } from 'main/example/services';

export function ExampleListPage(): ReactElement {
  const history = useHistory();
  const queryString = useQueryString();
  const searchParams = queryString.objectify(history.location.search);

  const handleResourceListItemUpsert = () => {
    history.push(history.location);
  };

  const handleResourceListSearchParamsChange = (event) => {
    history.push({ ...history.location, search: queryString.stringify(event.searchParams) });
  };

  return (
    <ResourceList
      controlsRender={ExampleControls}
      itemRender={ExampleItem}
      searchParams={searchParams}
      service={exampleService}
      onItemUpsert={handleResourceListItemUpsert}
      onSearchParamsChange={handleResourceListSearchParamsChange}
    />
  );
}
