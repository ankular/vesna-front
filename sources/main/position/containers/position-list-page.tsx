import { useQueryString } from 'modules/hooks';
import { ResourceList } from 'modules/resource';
import React, { ReactElement } from 'react';
import { useHistory } from 'react-router';

import { PositionControls, PositionItem } from 'main/position/components';
import { positionService } from 'main/position/services';

export function PositionListPage(): ReactElement {
  const history = useHistory();
  const queryString = useQueryString();
  const searchParams = queryString.objectify(history.location.search);

  const handleResourceListItemUpsert = () => {
    history.push(history.location);
  };

  const handleResourceListSearchParamsChange = (event) => {
    history.push({ ...history.location, search: queryString.stringify(event.searchParams) });
  };

  return (
    <ResourceList
      controlsRender={PositionControls}
      itemRender={PositionItem}
      searchParams={searchParams}
      service={positionService}
      onItemUpsert={handleResourceListItemUpsert}
      onSearchParamsChange={handleResourceListSearchParamsChange}
    />
  );
}
