import { ResourceMultiSelect, ResourceMultiSelectProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { PositionControls } from 'main/position/components/position-controls';
import { PositionItem } from 'main/position/components/position-item';
import { Position } from 'main/position/models';
import { positionService } from 'main/position/services';

export interface PositionMultiSelectProps extends Omit<ResourceMultiSelectProps<Position>, 'service'> {
}

export function PositionMultiSelect(props: PositionMultiSelectProps): ReactElement {
  return (
    <ResourceMultiSelect
      {...props}
      controlsRender={PositionControls}
      itemRender={PositionItem}
      service={positionService}
    />
  );
}
