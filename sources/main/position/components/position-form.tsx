import { Input } from '@progress/kendo-react-inputs';
import { FormGroup } from 'modules/components';
import { ResourceForm, ResourceFormProps } from 'modules/resource';
import React, { Fragment, ReactElement } from 'react';

import { Position } from 'main/position/models';
import { positionService } from 'main/position/services';

export interface PositionFormProps extends Omit<ResourceFormProps<Position>, 'service'> {
}

export function PositionForm(props: PositionFormProps): ReactElement {
  return (
    <ResourceForm
      {...props}
      service={positionService}
      children={({ submitted, value, setValue }) => (
        <Fragment>
          <FormGroup label="Название">
            <Input
              className="w-100"
              disabled={submitted}
              required
              value={value.name ?? ''}
              onChange={(e) => setValue({ ...value, name: e.target.value })}
            />
          </FormGroup>

          <FormGroup label="Описание">
            <textarea
              className="k-textarea w-100"
              disabled={submitted}
              value={value.description ?? ''}
              onChange={(e) => setValue({ ...value, description: e.target.value })}
            />
          </FormGroup>
        </Fragment>
      )}
    />
  );
}
