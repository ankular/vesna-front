import { ResourceControls, ResourceControlsProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { PositionForm } from 'main/position/components/position-form';
import { Position } from 'main/position/models';
import { positionService } from 'main/position/services';

export interface PositionControlsProps extends Omit<ResourceControlsProps<Position>, 'service'> {
}

export function PositionControls(props: PositionControlsProps): ReactElement {
  return (
    <ResourceControls
      {...props}
      formRender={PositionForm}
      service={positionService}
    />
  );
}
