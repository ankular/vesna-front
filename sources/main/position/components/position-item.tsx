import { ResourceItem, ResourceItemProps } from 'modules/resource';
import React, { ReactElement } from 'react';

import { Position } from 'main/position/models';
import { positionService } from 'main/position/services';

export interface PositionItemProps extends Omit<ResourceItemProps<Position>, 'service'> {
}

export function PositionItem(props: PositionItemProps): ReactElement {
  return (
    <ResourceItem
      {...props}
      service={positionService}
    />
  );
}
