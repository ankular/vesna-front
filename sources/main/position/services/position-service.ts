import { createResourceService } from 'modules/resource';

import { Position } from 'main/position/models';

const service = createResourceService<Position>({
  baseURL: '/api/positions',
  dataItemKey: 'id',
  scope: 'employees()',
  textField: 'name',
});

export const positionService = Object.assign(service, {});
