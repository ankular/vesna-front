import { Employee } from 'main/employee/models';

export interface Position {
  id?: number;

  description?: string;
  name?: string;

  employees?: Employee[];
}
