const { spawn } = require('child_process');
const CompressionPlugin = require('compression-webpack-plugin');
const { config: dotenvConfig } = require('dotenv');
const HtmlPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const { join } = require('path');
const TerserPlugin = require('terser-webpack-plugin');
const DefinePlugin = require('webpack/lib/DefinePlugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const CopyPlugin = require('copy-webpack-plugin');

dotenvConfig();

const isProdEnv = process.env.NODE_ENV === 'production';

const paths = {
  context: join(__dirname, 'sources'),
  static: join(__dirname, 'static'),
  output: join(__dirname, 'bundle'),
};

const webpackConfig = {
  context: paths.context,
  devServer: {
    contentBase: paths.output,
    historyApiFallback: true,
    host: process.env.HOST,
    port: process.env.PORT,
    proxy: {
      '/api': {
        changeOrigin: true,
        target: process.env.PROXY,
      },
    },
  },
  devtool: isProdEnv ? false : 'source-map',
  entry: '.',
  mode: process.env.NODE_ENV,
  module: {
    rules: [
      {
        exclude: /node_modules/,
        test: /\.tsx?$/,
        use: 'ts-loader',
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          // 'postcss-loader',
          'sass-loader',
        ],
      },
      {
        test: /\.(png|jpg|gif|svg|eot|ttf|woff|otf)([?]?.*)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8192,
              name: join('assets', '[name].[hash].[ext]'),
            },
          },
        ],
      },
    ],
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
      name(module, chunks, cacheGroupKey) {
        const packageName = module
          .identifier()
          .replace(/.*node_modules\/([^/]*).*/i, '$1')
          .replace(/[^a-z]*/i, '');

        if (/react/i.test(packageName)) {
          return 'react';
        }

        if (['progress', 'telerik'].includes(packageName)) {
          return 'progress';
        }

        if (['bootstrap', 'jquery'].includes(packageName)) {
          return 'bootstrap';
        }

        if (/polyfill/i.test(packageName) || ['core-js'].includes(packageName)) {
          return 'polyfills';
        }

        return cacheGroupKey;
      },
    },
  },
  output: {
    filename: 'scripts/[name].[hash].js',
    path: paths.output,
    publicPath: '/',
  },
  performance: {
    hints: false,
  },
  plugins: [
    {
      apply(compiler) { // чистит папку билка про production сборке
        spawn('rm', ['-rf', compiler.options.output.path]);
      },
    },
    new DefinePlugin({
      'process.env': JSON.stringify(process.env),
    }),
    new HtmlPlugin({
      template: join(paths.context, 'index.html'),
    }),
    new MiniCssExtractPlugin({
      filename: 'styles/[name].[hash].css',
    }),
    new CopyPlugin([
      { from: paths.static, to: paths.output }
    ]),
  ],
  resolve: {
    alias: {
      'common': join(paths.context, 'common'),
      'main': join(paths.context, 'main'),
      'modules': join(paths.context, 'modules'),
    },
    extensions: [
      '.tsx',
      '.ts',
      '.js',
    ],
  },
};

if (isProdEnv) {
  webpackConfig.plugins.push(
    new CompressionPlugin(),
  );

  webpackConfig.optimization.minimize = true;

  webpackConfig.optimization.minimizer = [
    new TerserPlugin({
      extractComments: false,
      parallel: true,
      terserOptions: {
        output: {
          comments: false,
        },
      },
    }),
  ];
}

if (/^true$/i.test(process.env.STATS)) {
  webpackConfig.plugins.push(
    new BundleAnalyzerPlugin(),
  );
}

module.exports = webpackConfig;
